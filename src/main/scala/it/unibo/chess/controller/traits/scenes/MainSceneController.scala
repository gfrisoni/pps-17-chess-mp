package it.unibo.chess.controller.traits.scenes

import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.view.general.observers.MainSceneObserver
import it.unibo.chess.view.general.scenes.MainScene

trait MainSceneController extends MainSceneObserver with GenericController[MainScene]
