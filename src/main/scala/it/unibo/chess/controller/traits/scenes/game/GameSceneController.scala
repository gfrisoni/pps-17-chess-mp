package it.unibo.chess.controller.traits.scenes.game

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.GenericController
import it.unibo.chess.model.core.ChessGame
import it.unibo.chess.view.general.observers.{BoardViewObserver, DetailsViewObserver, GameSceneObserver}
import it.unibo.chess.view.general.scenes.GameScene

trait GameSceneController extends GameSceneObserver
  with BoardViewObserver with DetailsViewObserver with GenericController[GameScene] {

  protected var gameClock: Option[ChessClock] = None
  var game: ChessGame

  def getGameClock: Option[ChessClock] = gameClock
  def setGameClock(clock: ChessClock)

}




