package it.unibo.chess.controller.settings

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Represents the settings for Chess Multiplayer application.
  */
sealed trait AppSettings {

  /**
    * The settings related to MongoDB connection.
    */
  val mongoSettings: MongoSettings

}

/**
  * Handles the settings implementation for Chess Multiplayer application
  * and provides factories.
  */
object AppSettings {

  /**
    * Constructs the application settings from the specified configurations.
    *
    * @param mongoConfig the configuration to use for MongoDB settings
    * @return the application settings.
    */
  def apply(mongoConfig: Config): AppSettings = AppSettingsImpl(mongoConfig)

  /**
    * @return the application settings constructed from the standard default configurations.
    */
  def apply(): AppSettings = AppSettingsImpl(ConfigFactory.load())

  private case class AppSettingsImpl(mongoConfig: Config) extends AppSettings {

    override val mongoSettings: MongoSettings = MongoSettings(mongoConfig)

    override def toString: String = {
      s"""Mongo settings:
         |${mongoSettings.toString}""".stripMargin
    }

  }

}