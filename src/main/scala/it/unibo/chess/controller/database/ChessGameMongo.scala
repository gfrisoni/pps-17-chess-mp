package it.unibo.chess.controller.database

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.exceptions.Database.DatabaseException
import it.unibo.chess.exceptions.Database.Types.UnfinishedGameSavingAttempt
import it.unibo.chess.model.core.{Black, ChessGame, White}
import it.unibo.chess.model.pgn.PgnConverter
import org.mongodb.scala.bson.ObjectId

/**
  * Defines the representation of the chess games documents to use inside the MongoDB collection.
  *
  * @param _id the primary key of the game document
  * @param players the players of the game
  * @param result the ending action of the game
  * @param dateTime the date on which the game takes place
  * @param pgn the PGN representation of the game
  * @param views the number of views associated to the game
  * @param nMoves the number of moves performed in the game
  * @param duration the duration of the game (expressed in seconds)
  */
case class ChessGameMongo(_id: ObjectId,
                          players: Map[String, String],
                          dateTime: String,
                          result: String,
                          pgn: String,
                          views: Long,
                          nMoves: Long,
                          duration: Long)

/**
  * The companion object of ChessGameMongo.
  */
object ChessGameMongo {

  /**
    * Creates a new instance of the game document and automatically assign a primary key for it.
    *
    * @param game the chess game
    * @param gameClock the chess clock related to the game
    * @return a document that represents {{{game}}}.
    */
  def apply(game: ChessGame, gameClock: ChessClock): ChessGameMongo = {
    PgnConverter.resultToPgn(game.history) match {
      case Some(result) => ChessGameMongo(
        new ObjectId(),
        game.players.map{ case (color, player) => (color.toString, player.name) },
        game.dateTime.toString("yyyy.MM.dd"),
        result,
        PgnConverter.gameToPgn(game),
        0L,
        game.history.movesNumber,
        Seq(White, Black).foldLeft[Long](0) {(intermediate, player) =>
          intermediate + (gameClock.startingTimers(player) - gameClock.getTimerValues(player)).toSeconds}
      )
      case _ => throw DatabaseException("Cannot save an unfinished chess game with", UnfinishedGameSavingAttempt)
    }
  }

}