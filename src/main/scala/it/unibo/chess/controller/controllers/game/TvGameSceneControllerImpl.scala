package it.unibo.chess.controller.controllers.game

import it.unibo.chess.controller.traits.scenes.game.TvGameSceneController
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.core.{ChessColor, ChessGame, ChessRoles}
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent

/**
  * Implementation of the AbstractGameSceneController, when the game needs to be read-only
  * @param chessGame Game to manage
  */
case class TvGameSceneControllerImpl(chessGame: ChessGame) extends AbstractGameSceneController(chessGame) with TvGameSceneController {

  override def onDrawApplication(color: ChessColor, status: Boolean): Unit = {}
  override def onSquareSelected(boardPosition: BoardPosition): Unit = {}
  override def onPromotionSelected(boardPosition: BoardPosition, role: ChessRoles.ChessRole)(implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {}
  override def onPieceMove(from: BoardPosition, to: BoardPosition)(implicit doOnSuccess: () => Unit, doOnFail: String => Unit): Unit = {}
  override def onViewLoadCompletion(): Unit = {
    view.get.disablePlayerWaiting()
  }

  override def onExitGame(isSaveEnabled: Boolean): Unit = {
    view.foreach(view => view.windowManager.setScene(new Intent(SceneType.TvScene)))
  }

}
