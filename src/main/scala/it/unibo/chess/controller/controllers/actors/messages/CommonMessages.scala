package it.unibo.chess.controller.controllers.actors.messages

import it.unibo.chess.model.core.ChessRoles.ChessRole

object CommonMessages {

  final case class Move(fromPos: Symbol, toPos: Symbol, ownTime: Long, opponentTime: Long)

  final case class MoveFeedback(err: Option[String])

  final case class Promote(position: Symbol, newRole: ChessRole)

  final case class Draw(status: Boolean)

}
