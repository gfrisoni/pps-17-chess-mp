package it.unibo.chess.controller.controllers

import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scenes.MultiPlayerGameCreatorSceneController
import it.unibo.chess.model.core.{ChessPlayer, _}
import it.unibo.chess.model.modes.ChessModes
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.{ChallengerGameSceneIntent, CreatorGameSceneIntent, Intent}
import it.unibo.chess.view.general.scenes.gameCreator.{MultiPlayerGameCreatorScene, PlayerRole}

import scala.concurrent.duration.FiniteDuration

case class MultiPlayerGameCreatorSceneControllerImpl() extends MultiPlayerGameCreatorSceneController {

  override def onBack(): Unit = {
    require(view.isDefined)
    view.get.windowManager.setScene(new Intent(SceneType.MainScene))
  }

  override def onStart(): Unit = {
    require(view.isDefined)

    view.get.getSelectedPlayerRole match {
      case PlayerRole.Challenger =>
        val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer(), Black -> ChessPlayer(view.get.getPlayerName))
        val game: ChessGame = ChessGame(players = players)
        view.get.windowManager.setScene(new ChallengerGameSceneIntent(SceneType.GameScene, game))

      case PlayerRole.Creator =>
        val players: Map[ChessColor, ChessPlayer] = Map(White -> ChessPlayer(view.get.getPlayerName), Black -> ChessPlayer())
        val gameMode: ChessMode = view.get.getSelectedGameMode
        val time: FiniteDuration = view.get.getSelectedTime
        val game: ChessGame = ChessGame(players = players)
        val clock: ChessClock = ChessClock(gameMode, Map(White -> time, Black -> time))
        view.get.windowManager.setScene(new CreatorGameSceneIntent(SceneType.GameScene, game, Some(clock)))
    }
  }

  override def setView(view: MultiPlayerGameCreatorScene): Unit = {
    super.setView(view)
    this.view.get.setSelectableGameModes(ChessModes.chessModes)
  }
}