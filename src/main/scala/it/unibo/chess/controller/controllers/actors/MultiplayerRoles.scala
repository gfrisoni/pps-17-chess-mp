package it.unibo.chess.controller.controllers.actors

import akka.actor.{ActorRef, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import it.unibo.chess.controller.database.ChessGameDatabaseFunctions
import it.unibo.chess.controller.traits.scenes.game.GameSceneController

/**
  * The possible roles on multiplayer actors.
  */
object MultiplayerRoles {

  private val systemName: String = "MultiplayerActor"

  trait MultiplayerRole {
    /**
      * The config name of the actor, also used as actor name.
      */
    val configName: String
    /**
      * The type of the actor used upon its creation.
      */
    val actorType: Class[_ <: MultiplayerActor]
  }

  /**
    * Creator actor specifics.
    */
  case object Creator extends MultiplayerRole {
    override val configName: String = "creator"
    override val actorType: Class[_ <: MultiplayerActor] = classOf[CreatorActor]
  }

  /**
    * Challenger actor specifics.
    */
  case object Challenger extends MultiplayerRole {
    override val configName: String = "challenger"
    override val actorType: Class[_ <: MultiplayerActor] = classOf[ChallengerActor]
  }

  /**
    * Method used to create an actor.
    *
    * @param controller the related controller
    * @param dbFunctions the database functions to use
    * @return the created actor.
    */
  def launchActor(role: MultiplayerRole)(controller: GameSceneController, dbFunctions: ChessGameDatabaseFunctions): ActorRef = {
    getSystem.actorOf(Props(role.actorType, controller, dbFunctions), role.configName)
  }

  private def getSystem: ActorSystem = {
    ActorSystem(systemName, ConfigFactory.load.getConfig("akkaConfig"))
  }

}