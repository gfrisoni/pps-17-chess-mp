package it.unibo.chess.controller.controllers.actors

import akka.actor.Terminated
import it.unibo.chess.controller.controllers.actors.messages.ChallengerMessages.{DiscoveryCompleted, LaunchChallenge, UploadAgreement}
import it.unibo.chess.controller.controllers.actors.messages.ControllerMessages.SetUploadAgreement
import it.unibo.chess.controller.controllers.actors.messages.CreatorMessages.{ChallengeAccepted, UpdateStatus}
import it.unibo.chess.controller.controllers.game.MultiplayerGameSceneControllerImpl
import it.unibo.chess.controller.database.ChessGameDatabaseFunctions
import it.unibo.chess.controller.utilities.AddressUtilities
import it.unibo.chess.model.core._
import org.mongodb.scala.Completed

import scala.collection.mutable

/**
  * The implementation of the Creator actor, who opens the game first.
  *
  * @param controller its controller
  * @param dbFunctions its database functions
  */
class CreatorActor(override val controller: MultiplayerGameSceneControllerImpl,
                   override val dbFunctions: ChessGameDatabaseFunctions) extends AbstractMultiplayerActor(controller, dbFunctions) {

  val color: ChessColor = White
  val uploadPolicy: mutable.Map[ChessColor, Option[Boolean]] = mutable.Map(White -> None, Black -> None)
  controller.setActorAddressAndPort(getAddress, getPort)

  override def initialBehaviour: Receive = {
    case LaunchChallenge =>
      sender ! ChallengeAccepted(controller.game.players(color).name, controller.getGameClock.get.timerStrategy.chessMode, controller.getGameClock.get.startingTimers(Black)._1.toInt)
    case DiscoveryCompleted(opponentName) =>
      startGame(opponentName)
      context.become(gameBehaviour)
    case Terminated(deadActor) if deadActor == opponentRef.get => controller.onActorDead()
  }

  override def finalBehaviour: Receive = {
    case UploadAgreement(a) => uploadAgreement(!color, a)
    case SetUploadAgreement(a) => uploadAgreement(color, a)
    case Terminated(deadActor) if deadActor == opponentRef.get => uploadPolicy(!color) = Some(false)
  }

  /**
    * The player can decide if he want his game uploaded, there are 3 possible cases:
    * - 2 false: game is not uploaded.
    * - 1 true 1 false: the false player's name is anonymized.
    * - 2 true: game is uploaded with both players' names.
    *
    * @param color the player color
    * @param agreement if the player wants to upload
    */
  override def uploadAgreement(color: ChessColor, agreement: Boolean): Unit = {
    uploadPolicy(color) = Some(agreement)
    if (uploadPolicy.values.forall(_.isDefined)) {
      if (uploadPolicy.values.exists(_.get)) {
        val privacyPlayers = controller.game.players.map(p => if (!uploadPolicy(p._1).get) p._1 -> ChessPlayer() else p)
        upload(privacyPlayers, dbFunctions)
      } else {
        opponentRef.get ! UpdateStatus(isSavingEnabled = false)
        controller.onUploadCompleted(isSavingEnabled = false)
      }
    }
  }

  // The current system address (used by the view to display the Address)
  private def getAddress: String = AddressUtilities.addressOf(context.system)

  // The current system port (used by the view to display the Port)
  private def getPort: Int = AddressUtilities.portOf(context.system)

  // Uploads the game into the remote database
  private def upload(players: Map[ChessColor, ChessPlayer], dbFunctions: ChessGameDatabaseFunctions): Unit = {
    val updateGame: ChessGame = ChessGame.copy(controller.game)(newPlayers = players)
    dbFunctions.saveGame(updateGame, controller.getGameClock.get).subscribe(
      (result: Completed) => println(s"onNext: $result"),
      (e: Throwable) => {
        // Launch errors only to players that WANTS to save a game
        opponentRef.get ! UpdateStatus(uploadPolicy(Black).get, s"An error occurred during the saving of the game: $e")
        controller.onUploadCompleted(uploadPolicy(White).get, s"An error occurred during the saving of the game: $e")
      },
      () => {
        // Inform only the players that WANTS to save about saving
        opponentRef.get ! UpdateStatus(isSavingEnabled = uploadPolicy(Black).get, "This game was saved successfully! You can watch it on the TvMode!")
        controller.onUploadCompleted(isSavingEnabled = uploadPolicy(White).get, "This game was saved successfully! You can watch it on the TvMode!")
      }
    )
  }

}