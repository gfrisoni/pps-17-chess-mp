package it.unibo.chess.controller.scheduling

import akka.actor.Cancellable
import it.unibo.chess.controller.traits.scheduling.ChessClockObserver
import it.unibo.chess.controller.utilities.SchedulingUtilities
import it.unibo.chess.model.core.{Black, ChessColor, White}
import it.unibo.chess.controller.utilities.SchedulingUtilities._
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.controller.scheduling.TimeHandler.RelTime.toRelTime
import it.unibo.chess.exceptions.Timer.Types.{SwitchOnOwnTurn, TimerException, WhiteSwitchedFirst}

import scala.concurrent.duration._

/**
  * Represents a chess clock.
  */
sealed trait ChessClock {

  /**
    * The chess mode to use with the clock.
    */
  val timerStrategy: ChessClockStrategy

  /**
    * The starting values for the timers of the players.
    */
  val startingTimers: Map[ChessColor, FiniteDuration]

  /**
    * The scheduler to use for time handling.
    */
  val scheduler: AdvancedScheduler

  /**
    * Adds a new clock observer.
    *
    * @param observer the observer to register
    */
  def addObserver(observer: ChessClockObserver): Unit

  /**
    * Removes a registered clock observer.
    *
    * @param observer the observer to remove
    */
  def removeObserver(observer: ChessClockObserver): Unit

  /**
    * @return the current values of the chess clock timers.
    */
  def getTimerValues: Map[ChessColor, FiniteDuration]

  /**
    * Sets the timers of the chess clock.
    *
    * @param newTimers the new values for the clock timers
    */
  def setTimerValues(newTimers: Map[ChessColor, FiniteDuration]): Unit

  /**
    * Stops the chess clock.
    */
  def stop(): Unit

  /**
    * Starts the black timer updating.
    */
  def whitePressure(): Unit

  /**
    * Starts the white timer updating.
    */
  def blackPressure(): Unit

}

/**
  * The companion object of ChessClock.
  * It provides factories.
  */
object ChessClock {

  /**
    * Creates a new chess clock.
    *
    * @param chessMode the chess mode to consider for time updating
    * @param startingTimers the starting values for the timers related to the players
    * @param scheduler the scheduler to use
    * @return the new chess clock.
    */
  def apply(
             chessMode: ChessMode,
             startingTimers: Map[ChessColor, FiniteDuration],
             scheduler: AdvancedScheduler = AdvancedScheduler(SchedulingUtilities.actorSystem.scheduler)
           ): ChessClock = {
    ChessClockImpl(chessMode, startingTimers, scheduler)
  }

  /**
    * Creates a new chess clock from an existing one.
    *
    * @param chessClock the original chess clock to copy
    * @param newChessMode the chess mode of the new chess clock
    * @param newStartingTimers the starting timers of the new chess clock
    * @param newScheduler the scheduler of the new chess clock
    * @return the new chess clock.
    */
  def copy(chessClock: ChessClock)
          (newChessMode: ChessMode = chessClock.timerStrategy.chessMode,
           newStartingTimers: Map[ChessColor, FiniteDuration] = chessClock.startingTimers,
           newScheduler: AdvancedScheduler = chessClock.scheduler): ChessClock = {
    ChessClockImpl(newChessMode, newStartingTimers, newScheduler)
  }

  private case class ChessClockImpl(
                                     chessMode: ChessMode,
                                     override val startingTimers: Map[ChessColor, FiniteDuration],
                                     override val scheduler: AdvancedScheduler
                                   ) extends ChessClock {

    override val timerStrategy = ChessClockStrategy(chessMode)

    var turn: Option[ChessColor] = None
    var currentTimerTask: Option[Cancellable] = None

    var timers: Map[ChessColor, FiniteDuration] = startingTimers

    var observers: Set[ChessClockObserver] = Set.empty

    override def addObserver(observer: ChessClockObserver): Unit = observers = observers + observer

    override def removeObserver(observer: ChessClockObserver): Unit = observers = observers - observer

    override def getTimerValues: Map[ChessColor, FiniteDuration] = timers

    override def setTimerValues(newTimers: Map[ChessColor, FiniteDuration]): Unit = timers = newTimers

    override def stop(): Unit = currentTimerTask.map(_.cancel())

    // Handles the timers on switch event
    private def switch(nextTurn: ChessColor): Unit = {
      // Stops the timer of the current player and starts the opponent's one
      def _switch(): Unit = {
        stop()
        turn = Some(nextTurn)
        timers = timerStrategy.updateTimersOnSwitch(timers, nextTurn)
        currentTimerTask = Some(scheduler.doRegularly(
          {
            timers = timerStrategy.updateTimersOnTick(timers, nextTurn)
            timers
              .find(_._2 <= 0.second)
              .fold(observers.foreach(_.onTick(timers))) { t => observers.foreach(_.onTimeout(t._1)); stop() }
          })(1.second.rel))
      }
      turn.fold(if (nextTurn == Black) throw TimerException("White player cannot start", WhiteSwitchedFirst) else _switch()) {
        case `nextTurn` => throw TimerException(s"It is already $nextTurn's turn", SwitchOnOwnTurn)
        case _ => _switch()
      }
    }

    override def whitePressure(): Unit = switch(Black)

    override def blackPressure(): Unit = switch(White)

  }

}
