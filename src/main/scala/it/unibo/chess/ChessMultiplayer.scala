package it.unibo.chess

import it.unibo.chess.view.general.intents.Intent
import javafx.application.{Application, Platform}
import javafx.stage.Stage
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.{FXWindow, GenericWindow}

class ChessMultiplayer extends Application {

  override def start(primaryStage: Stage): Unit = {
      Platform.runLater(() => {
        // Create the manager for the window
        val view: GenericWindow = FXWindow(primaryStage)
        view.setTitle("Chess MP")
        view.setScene(new Intent(SceneType.MainScene))
        view.showView()
    })
  }

}

object ChessMultiplayer extends App {
  Application.launch(classOf[ChessMultiplayer], args: _*)
}
