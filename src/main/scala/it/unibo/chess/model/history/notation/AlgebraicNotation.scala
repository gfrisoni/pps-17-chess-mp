package it.unibo.chess.model.history.notation

import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.KingStates.{Check, Checkmate}
import it.unibo.chess.model.core.{AmbiguityTypes, White}
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.history._
import it.unibo.chess.model.history.notation.CaptureNotations._
import it.unibo.chess.model.history.notation.CastlingNotations._
import it.unibo.chess.model.history.notation.CheckNotations._
import it.unibo.chess.model.history.notation.CheckmateNotations._
import it.unibo.chess.model.history.notation.FromPosRepresentationModes.FromPosRepresentationMode
import it.unibo.chess.model.history.notation.PromotionNotations._
import it.unibo.chess.model.utilities.ChessUtilities._

/**
  * Defines the supported capture notations for AN.
  */
object CaptureNotations {

  sealed trait CaptureNotation

  case object XCaptureNotation extends CaptureNotation
  case object ColonCaptureNotation extends CaptureNotation
  case object OmittedCaptureNotation extends CaptureNotation

  val captureNotations = Set(XCaptureNotation, ColonCaptureNotation, OmittedCaptureNotation)

}

/**
  * Defines the supported castling notations for AN.
  */
object CastlingNotations {

  sealed trait CastlingNotation

  case object ZeroesCastlingNotation extends CastlingNotation
  case object OsCastlingNotation extends CastlingNotation
  case object WordCastlingNotation extends CastlingNotation

  val castlingNotations = Set(ZeroesCastlingNotation, OsCastlingNotation, WordCastlingNotation)

}

/**
  * Defines the supported promotion notations for AN.
  */
object PromotionNotations {

  sealed trait PromotionNotation

  case object EqualsPromoteNotation extends PromotionNotation
  case object ParenthesisPromoteNotation extends PromotionNotation

  val promotionNotations = Set(EqualsPromoteNotation, ParenthesisPromoteNotation)

}

/**
  * Defines the supported check notations for AN.
  */
object CheckNotations {

  sealed trait CheckNotation

  case object PlusCheckNotation extends CheckNotation
  case object DaggerCheckNotation extends CheckNotation
  case object WordCheckNotation extends CheckNotation

  val checkNotations = Set(PlusCheckNotation, DaggerCheckNotation, WordCheckNotation)

}

/**
  * Defines the supported checkmate notations for AN.
  */
object CheckmateNotations {

  sealed trait CheckmateNotation

  case object HashtagCheckmateNotation extends CheckmateNotation
  case object DoublePlusCheckmateNotation extends CheckmateNotation
  case object DoubleDaggerCheckmateNotation extends CheckmateNotation
  case object WordCheckmateNotation extends CheckmateNotation
  
  val checkmateNotations = Set(HashtagCheckmateNotation, DoublePlusCheckmateNotation,
    DoubleDaggerCheckmateNotation, WordCheckmateNotation)

}

/**
  * Defines the representation modes for the starting position required by AN.
  */
object FromPosRepresentationModes {

  sealed trait FromPosRepresentationMode

  case object OnlyFile extends FromPosRepresentationMode
  case object OnlyRaw extends FromPosRepresentationMode
  case object All extends FromPosRepresentationMode

}

/**
  * An algebraic notation (AN) with custom rules.
  *
  * @param longNotation if {{{true}}}, uses LAN
  * @param lowerCaseFiles if {{{true}}}, uses lower case for files representation
  * @param figurine if {{{true}}}, uses figurines for pieces representation (FAN)
  * @param captureNotation the preference for the capture notation
  * @param castlingNotation the preference for the castling notation
  * @param promotionNotation the preference for the promotion notation
  * @param checkNotation the preference for the check notation
  * @param checkmateNotation the preference for the checkmate notation
  */
case class AlgebraicNotationRules(longNotation: Boolean,
                                  lowerCaseFiles: Boolean,
                                  figurine: Boolean,
                                  captureNotation: CaptureNotation,
                                  castlingNotation: CastlingNotation,
                                  promotionNotation: PromotionNotation,
                                  checkNotation: CheckNotation,
                                  checkmateNotation: CheckmateNotation) extends NotationRules {

  override val title: String = "Algebraic Notation"

  override val description: String =
    s"""Algebraic Notation (using ${if (lowerCaseFiles) "lowercase" else "uppercase"} letters for files,
       | ${if (figurine) "using" else "not using"} figurines for pieces,
       | ${if (captureNotation == CaptureNotations.XCaptureNotation) "distinguishing captures with x"
          else if (captureNotation == CaptureNotations.ColonCaptureNotation) "distinguishing captures with colon"
          else "not distinguishing captures"},
       | "using" ${if (castlingNotation == CastlingNotations.ZeroesCastlingNotation) "zeroes"
          else if (castlingNotation == CastlingNotations.OsCastlingNotation) "O"
          else "castle word"} for castling,
       | using ${if (promotionNotation == PromotionNotations.EqualsPromoteNotation) "="
          else "parenthesis"} for promotions,
       | using ${if (checkNotation == CheckNotations.PlusCheckNotation) "+"
          else if (checkNotation == CheckNotations.DaggerCheckNotation) "†"
          else "ch abbreviation"} for checks,
       | using ${if (checkmateNotation == CheckmateNotations.HashtagCheckmateNotation) "#"
          else if (checkmateNotation == CheckmateNotations.DoublePlusCheckmateNotation) "++"
          else if (checkmateNotation == CheckmateNotations.DoubleDaggerCheckmateNotation) "‡"
          else "mate abbreviation"} for checkmates
     """.stripMargin

}

/**
  * Defines an algebraic notation (AN) with all its possible configurations.
  */
object AlgebraicNotation extends Notation[AlgebraicNotationRules] {

  override def allPossibleRules: Set[AlgebraicNotationRules] = {
    for {
      longNotation <- Set(true, false)
      lowerCaseLetters <- Set(true, false)
      figurine <- Set(true, false)
      captureNotation <- CaptureNotations.captureNotations
      castlingNotation <- CastlingNotations.castlingNotations
      promoteNotation <- PromotionNotations.promotionNotations
      checkNotation <- CheckNotations.checkNotations
      checkmateNotation <- CheckmateNotations.checkmateNotations
    } yield
      AlgebraicNotationRules(
        longNotation,
        lowerCaseLetters,
        figurine,
        captureNotation,
        castlingNotation,
        promoteNotation,
        checkNotation,
        checkmateNotation)
  }

}

/**
  * Handles an AN serializer.
  *
  * @param rules the specific rules to use during serialization
  */
case class AlgebraicNotationGameActionSerializer(rules: AlgebraicNotationRules) extends ChessGameActionSerializer {

  private val LONG_AN_SEPARATOR: String = "-"

  override protected def move(action: MoveAction): String = {
    fromPiece(action) +
      (if (rules.longNotation) fromPos(action) + LONG_AN_SEPARATOR
      else fromPosByAmbiguity(action)) +
      toPos(action) + checkAndCheckmate(action)
  }

  override protected def capture(action: CaptureAction): String = {
    fromPiece(action) +
      (if (rules.longNotation) fromPos(action)
      else if (action.from._1.role == Pawn(action.from._1.color)) fromPos(action, FromPosRepresentationModes.OnlyFile)
      else fromPosByAmbiguity(action)) +
      captureDash + toPos(action) + checkAndCheckmate(action)
  }

  override protected def capturePromote(action: CapturePromoteAction): String = {
    (if (rules.longNotation) fromPos(action)
    else fromPos(action, FromPosRepresentationModes.OnlyFile)) +
      captureDash + toPos(action) + promotionPiece[CapturePromoteAction](action) + checkAndCheckmate(action)
  }

  override protected def enPassant(action: EnPassantAction): String = {
    (if (rules.longNotation) fromPos(action)
    else fromPos(action, FromPosRepresentationModes.OnlyFile)) +
      captureDash + toPos(action) + "e.p." + checkAndCheckmate(action)
  }

  override protected def promote(action: PromoteAction): String = {
    (if (rules.longNotation) fromPos(action) + LONG_AN_SEPARATOR
    else fromPosByAmbiguity(action)) +
      toPos(action) + promotionPiece[PromoteAction](action) + checkAndCheckmate(action)
  }

  override protected def castling(action: CastlingAction): String = {
    rules.castlingNotation match {
      case ZeroesCastlingNotation => if (action.isKingSide) "0-0" else "0-0-0"
      case OsCastlingNotation => if (action.isKingSide) "O-O" else "O-O-O"
      case WordCastlingNotation => "castles"
    }
  }

  override protected def draw(action: DrawAction): String = "½-½"

  override protected def lose(action: LoseAction): String = if (action.loserColor == White) "0-1" else "1-0"

  private def anPieceNotation(role: ChessRole, figurine: Boolean): String = {
    role match {
      case king: King => if (figurine) AnUtilities.figurine(White, king) else "K"
      case Queen => if (figurine) AnUtilities.figurine(White, Queen) else "Q"
      case Rook => if (figurine) AnUtilities.figurine(White, Rook) else "R"
      case Bishop => if (figurine) AnUtilities.figurine(White, Bishop) else "B"
      case Knight => if (figurine) AnUtilities.figurine(White, Knight) else "N"
      case Pawn(_) =>  ""
    }
  }

  private def fromPiece(action: ChessMoveAction) = anPieceNotation(action.from._1.role, rules.figurine)

  private def fromPos(action: ChessMoveAction, mode: FromPosRepresentationMode = FromPosRepresentationModes.All) = {
    val boardPosition: String = mode match {
      case FromPosRepresentationModes.OnlyFile => action.from._2.toString.charAt(0).toString
      case FromPosRepresentationModes.OnlyRaw => action.from._2.toString.charAt(1).toString
      case _ => action.from._2.toString
    }
    if (rules.lowerCaseFiles) boardPosition else boardPosition.toUpperCase
  }

  private def fromPosByAmbiguity(action: ChessMoveAction, separator: String = ""): String = {
    action.ambiguity match {
      case AmbiguityTypes.SameFileAmbiguity => fromPos(action, FromPosRepresentationModes.OnlyRaw) + separator
      case AmbiguityTypes.SameRankAmbiguity => fromPos(action, FromPosRepresentationModes.OnlyFile) + separator
      case AmbiguityTypes.CompleteAmbiguity => fromPos(action) + separator
      case _ => ""
    }
  }

  private def toPos(action: ChessMoveAction) = {
    val boardPosition = action.toPosition.toString
    if (rules.lowerCaseFiles) boardPosition else boardPosition.toUpperCase
  }

  private def captureDash = {
    rules.captureNotation match {
      case XCaptureNotation => "x"
      case ColonCaptureNotation => ":"
      case OmittedCaptureNotation => ""
    }
  }

  private def promotionPiece[P <: ChessPromotionAction](action: P) = {
    rules.promotionNotation match {
      case ParenthesisPromoteNotation => "(" + anPieceNotation(action.promoteRole, rules.figurine) + ")"
      case EqualsPromoteNotation => "=" + anPieceNotation(action.promoteRole, rules.figurine)
    }
  }

  private def checkAndCheckmate(action: CheckableAction) = {
    if (action.kingState == Check) {
      rules.checkNotation match {
        case PlusCheckNotation => "+"
        case DaggerCheckNotation => "†"
        case WordCheckNotation => " ch"
      }
    } else if (action.kingState == Checkmate) {
      rules.checkmateNotation match {
        case HashtagCheckmateNotation => "#"
        case DoublePlusCheckmateNotation => "++"
        case DoubleDaggerCheckmateNotation => "‡"
        case WordCheckmateNotation => " mate"
      }
    } else ""
  }

}