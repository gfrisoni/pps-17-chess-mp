package it.unibo.chess.model.history.notation

import it.unibo.chess.model.history.BoardGameAction

/**
  * Represents the basic information for the rules of a notation.
  */
trait NotationRules {

  /**
    * The title related to the notation.
    */
  val title: String

  /**
    * The description of the current notation according to the selected rules.
    */
  val description: String

}

/**
  * Models a generic notation with a certain rules configuration.
  *
  * @tparam NR the specific type of notation rules
  */
abstract class Notation[NR <: NotationRules] {

  /**
    * @return a set with all the possible rules configurations.
    */
  def allPossibleRules: Set[NR]

}

/**
  * Models a serializer for the actions of a generic board game.
  *
  * @tparam A the specific type of board game action
  */
trait BoardGameActionSerializer[A <: BoardGameAction[_]] {

  /**
    * @return the rules configuration to use during the serialization.
    */
  def rules: NotationRules

  /**
    * Serializes a game action.
    *
    * @param action the action to serialize
    * @return the string resulting from the game action serialization.
    */
  def serializeGameAction(action: A): String

  /**
    * Serializes a collection of game actions.
    *
    * @param actions the actions to serialize
    * @return the collection of strings resulting from the game actions serialization.
    */
  def serializeGameActions(actions: A*): Seq[String]

}