package it.unibo.chess.model.history

import it.unibo.chess.model.core.Piece

/**
  * Represents a generic action for a board game.
  *
  * @tparam P the specific type of piece
  */
trait BoardGameAction[P <: Piece[_]]