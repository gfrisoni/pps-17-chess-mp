package it.unibo.chess.model.utilities

import scala.collection.immutable.NumericRange

/**
  * Handles the utilities for char formatting.
  */
object CharFormatUtilities {

  final val ASCII_FIRST_LETTER: Int = 96
  final val ALPHABET_LENGTH: Int = 26
  final val DIGITS: Int = 9
  final val COLUMNS: NumericRange[Char] = 'a' to 'g'

  /**
    * Converts a number into the correspondent letter of the alphabet.
    *
    * @param index index of a letter
    * @return the correspondent letter.
    */
  def intToLetter(index: Int): String = {
    if (index >= 0 && index <= ALPHABET_LENGTH)
      (CharFormatUtilities.ASCII_FIRST_LETTER + index).toChar.toString
    else
      throw new IndexOutOfBoundsException
  }

  /**
    * Converts a letter into the correspondent number.
    *
    * @param char the char to convert
    * @return the correspondent letter.
    */
  def letterToInt(char: String): Int = {
    if (char.length == 1 && COLUMNS.contains(char.charAt(0)))
      char.charAt(0).asDigit - DIGITS
    else
      throw new IndexOutOfBoundsException
  }

}
