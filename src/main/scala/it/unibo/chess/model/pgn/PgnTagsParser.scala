package it.unibo.chess.model.pgn

import scala.util.parsing.combinator.RegexParsers
import scalaz.ValidationNel
import scalaz._
import Scalaz._

/**
  * Handles a regex parser for PGN tags.
  * It converts a valid string into PGN tags.
  *
  * @see [[https://johnkurkowski.com/posts/accumulating-multiple-failures-in-a-validationnel/]] for the failure
  *     accumulation logic used by the parser.
  */
object PgnTagsParser extends RegexParsers {

  /**
    * The regex for the name of a PGN tag.
    */
  val tagName: Parser[String] = "[" ~> "[a-zA-Z]+".r

  /**
    * The regex for the value of a PGN tag.
    */
  val tagValue: Parser[String] = "\"" ~> """[^\]\r\n"]+""".r <~ """"](|\r|\n|\r\n)""".r

  /**
    * @return the parser for a single PGN tag.
    */
  def tag: Parser[ValidationNel[String, PgnTag]] = tagName ~ tagValue ^^ {
    case name ~ value => PgnTag(name, value)
  }

  /**
    * @return the parser for a sequence of PGN tags.
    */
  def tags: Parser[List[ValidationNel[String, PgnTag]]] = rep(tag)

  /**
    * @return the parser for a sequence of PGN tags, followed by the separator of the section related to the PGN moves.
    */
  def all: Parser[List[ValidationNel[String, PgnTag]]] = tags <~ "(|.|\r|\n|\r\n)*".r

  /**
    * Parses the specified PGN tags string.
    *
    * @param pgnTags the tags section inside the PGN string
    * @return the collection of PGN tags if the parsing is successful, a failure otherwise.
    */
  def apply(pgnTags: String): ValidationNel[String, PgnTags] = {val r = parseAll(all, pgnTags); r match {
    case Success(tags, _) =>
      // Accumulates both failures and successful results
      tags.foldLeft[ValidationNel[String, Seq[PgnTag]]](Seq.empty[PgnTag].successNel[String]) {
        case (acc, v) => (acc |@| v)(_ :+ _)
      } match {
        case scalaz.Success(validTags) => PgnTags(validTags).successNel
        case scalaz.Failure(error) =>
          raw"""Cannot parse PGN tags. Format errors detected:
               |${error.list.toList.mkString(sys.props("line.separator"))}""".stripMargin.failureNel
      }
    case NoSuccess(error, _) =>
      raw"""Cannot parse PGN tags.
         |An error occurred: $error""".stripMargin.failureNel
  }}

}
