package it.unibo.chess.model.pgn

/**
  * Represents an observer for an operation that consists of a conversion
  * from a PGN string to the object related to the associated game.
  */
trait FromPgnObserver {

  /**
    * Notifies the total number of moves detected inside the PGN string.
    *
    * @param nMoves the total number of moves
    */
  def totalMoves(nMoves: Int): Unit

  /**
    * Notifies completion of a PGN move parsing.
    *
    * @param san the move expressed in Standard Algebraic Notation
    */
  def moveParsed(san: String): Unit

}