package it.unibo.chess.model.pgn

import it.unibo.chess.model.pgn.PgnTagTypes.Result

import scala.language.postfixOps
import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers
import scalaz.ValidationNel
import scalaz.Scalaz._

/**
  * Handles a regex parser for PGN moves.
  * It converts a valid string into a tuple composed by a sequence of strings with the starting comments,
  * a sequence of parsed PGN moves and an optional PGN tag with the game result.
  */
object PgnMovesParser extends RegexParsers {

  override val whiteSpace: Regex = """(\s|\t|\r?\n)+""".r

  /**
    * The regex for a chess move expressed in Standard Algebraic Notation.
    */
  val move: Parser[String] = """0\-0\-0|0\-0|(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)""".r

  /**
    * The regex for a PGN tag move enumeration.
    */
  val number: Parser[String] = """[1-9]\d*[\s\.]*""".r

  /**
    * The regex for a block PGN comment.
    */
  val blockCommentary: Parser[String] = "{" ~> """[^\}]*""".r <~ "}"

  /**
    * The regex for an inline PGN comment.
    */
  val inlineCommentary: Parser[String] = ";" ~> """.+""".r

  /**
    * The regex for an any kind of PGN comment.
    */
  val commentary: Parser[String] = blockCommentary | inlineCommentary

  /**
    * The regex for a PGN game result.
    */
  val result: Parser[String] = "*" | "1/2-1/2" | "½-½" | "0-1" | "1-0"

  def moveExtras: Parser[Unit] = commentary.^^^(())

  private def cleanComments(comments: Seq[String]) = comments.map(_.trim).filter(_.nonEmpty)

  /**
    * @return the parser for a single PGN move.
    */
  def pgnMove: Parser[PgnMove] = (number?) ~> (move ~ rep(commentary)) <~ (moveExtras*) ^^ {
    case san ~ comments => PgnMove(san, cleanComments(comments))
  }

  /**
    * @return the parser for a sequence of PGN moves.
    */
  def pgnMoves: Parser[(Seq[String], Seq[PgnMove], Option[String])] = (commentary*) ~ (pgnMove*) ~ (result?) ~ (commentary*) ^^ {
    case comments ~ sans ~ res ~ _ => (comments, sans, res)
  }

  /**
    * Parses the specified PGN moves string.
    *
    * @param pgn the moves section inside the PGN string
    * @return a tuple with the starting comments, the parsed moves with their comments and the game result if present.
    */
  def apply(pgn: String): ValidationNel[String, (Seq[String], Seq[PgnMove], Option[PgnTag])] = parseAll(pgnMoves, pgn) match {
    case Success((comments, moves, res), _) =>
      res.fold[ValidationNel[String, (Seq[String], Seq[PgnMove], Option[PgnTag])]]((comments, moves, None).successNel){ r =>
        PgnTag(Result, r) match {
          case scalaz.Success(tag) => (comments, moves, Some(tag)).successNel
          case scalaz.Failure(error) =>
            raw"""Cannot parse ending result inside PGN moves section.
                 |An error occurred: $error""".stripMargin.failureNel
        }
      }
    case NoSuccess(error, _) =>
      raw"""Cannot parse PGN moves.
           |An error occurred: $error""".stripMargin.failureNel
  }

}