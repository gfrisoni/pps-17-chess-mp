package it.unibo.chess.model.pgn

import scala.language.postfixOps
import scala.util.Try
import scalaz.ValidationNel
import scalaz.syntax.validation._
import com.github.nscala_time.time.Imports._
import it.unibo.chess.model.core._
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.history.ChessActions.{ChessEndingAction, DrawAction, LoseAction}
import it.unibo.chess.model.history.ChessGameHistory
import it.unibo.chess.model.utilities.StringUtilities._
import it.unibo.chess.model.history.notation.AlgebraicNotationGameActionSerializer
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities.STANDARD_AN_RULES
import it.unibo.chess.model.utilities.ChessUtilities.PgnUtilities._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Handles the utilities for a chess PGN converter.
  */
object PgnConverter {

  /**
    * Converts a chess game ending result into the related PGN string, if registered inside the history.
    *
    * @param history the chess game history in which to look for the result
    * @return the PGN representation of the chess game result.
    */
  def resultToPgn(history: ChessGameHistory): Option[String] = {
    history.gameResult.fold[Option[String]](None)(res => Some(
      ChessGameHistory.copy(history)(newSerializer = AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES))
        .serializer
        .serializeGameAction(res)
    ))
  }

  // Serializes each game step related to a non ending action and groups them into enumerate full moves
  private def serializeAndSplitHistoryMoves(history: ChessGameHistory): Seq[(Seq[String], Int)] = {
    val pgnHistory: ChessGameHistory = ChessGameHistory.copy(history)(
      newSerializer = AlgebraicNotationGameActionSerializer(STANDARD_AN_RULES)
    )
    pgnHistory
      .gameSteps
      .filterNot(_.action.isInstanceOf[ChessEndingAction])
      .map(g => pgnHistory.serializer.serializeGameAction(g.action))
      .sliding(PGN_PERIOD_SIZE, PGN_PERIOD_SIZE)
      .toSeq
      .zipWithIndex
  }

  /**
    * Converts a chess game history into the related PGN string.
    *
    * @param history the chess game history
    * @return the PGN representation of the chess game history.
    */
  def historyToPgn(history: ChessGameHistory): String = {
    serializeAndSplitHistoryMoves(history)
      .collect({ case (s, i) => (i + 1) + PGN_INDEX_SEPARATOR + s.mkString(PGN_MOVE_SEPARATOR) })
      .mkString(PGN_PERIOD_SEPARATOR)
      .concat(resultToPgn(history).fold("")(res => PGN_MOVE_SEPARATOR + res))
  }

  /**
    * Converts a chess game history into an object representation of the related PGN.
    *
    * @param history the chess game history
    * @return a pair with the collection of turn indexes mapped into their PGN full moves
    *         and the optional game result.
    */
  def historyToInteractivePgn(history: ChessGameHistory): (Map[Int, PgnFullMove], Option[String]) = {
    val serializedMoves: Map[Int, PgnFullMove] = serializeAndSplitHistoryMoves(history)
      .collect({ case (s, i) => (i + 1) -> PgnFullMove(s.head, s.lift(1)) })
      .toMap
    (serializedMoves, resultToPgn(history))
  }

  /**
    * Converts a chess game into the related PGN string.
    *
    * @param game the chess game
    * @return the PGN representation of the chess game.
    */
  def gameToPgn(game: ChessGame): String = {
    val tags: String =
      s"""[Event "Casual Game"]
         |[Site "Chess Multiplayer"]
         |[Date "${game.dateTime.toString("yyyy.MM.dd")}"]
         |[Round "-"]
         |[White "${game.players(White).name}"]
         |[Black "${game.players(Black).name}"]
         |[Result "${game.history.gameResult match {
            case Some(DrawAction(_)) => "½-½"
            case Some(LoseAction(loser, _)) => if (loser == Black) "1-0" else "0-1"
            case _ => "*"
          }}"]""".stripMargin
    tags.concatIf(sys.props("line.separator") concat historyToPgn(game.history))(game.history.gameSteps.nonEmpty)
  }

  // Ensures the presence of a newline between the tags and the first move
  private def ensureTagsNewline(pgn: String): String =
    """"\]\s*(\d+\.)""".r.replaceAllIn(pgn, m => "\"]\n" + m.group(1))

  // Separates the pgn string into its tags and moves
  private def splitTagsAndMoves(pgn: String): (String, String) = {
    ensureTagsNewline(pgn).lines.toList.map(_.trim).filter(_.nonEmpty) span {
      line => line lift 0 contains '['
    } match {
      case (tagLines, moveLines) => (tagLines.mkString("\n"), moveLines.mkString("\n"))
    }
  }

  /**
    * Converts a pgn string into the related chess game.
    * If specified, it sends progress notifications to an observer.
    *
    * @param pgn the pgn string
    * @param observer the observer to notify
    * @return the chess game represented by the pgn string.
    */
  def fromPgn(pgn: String, observer: Option[FromPgnObserver] = None): Future[ChessGame] = Future {

    val preprocessed = pgn.lines
      .map(_.trim)
      .filter(!_.headOption.contains('%'))
      .mkString("\n")
      .replace("[pgn]", "")
      .replace("[/pgn]", "")
      .replace("‑", "-")
      .replace("e.p.", "") // Mutes en-passant notation

    val (tagStr, moveStr): (String, String) = splitTagsAndMoves(preprocessed)

    val parsedGame: ValidationNel[String, ChessGame] = PgnTagsParser(tagStr).fold(failure => s"Invalid PGN tags: ${failure.head}".failureNel, parsedTags => {
      if (parsedTags.containsSevenTagRoster) {
        PgnMovesParser(moveStr).fold(failure => s"Invalid PGN moves: ${failure.head}".failureNel, parsedMoves => {
          val moves: Seq[PgnMove] = parsedMoves._2
          observer.foreach(o => o.totalMoves(moves.length))
          val players: Map[ChessColor, ChessPlayer] = Map(
            White -> ChessPlayer(parsedTags.getTagValue(PgnTagTypes.White).get),
            Black -> ChessPlayer(parsedTags.getTagValue(PgnTagTypes.Black).get)
          )
          val date: DateTime = parsedTags.date.get
          Try(
            parsedTags
            .getTagValue(PgnTagTypes.Fen)
            .fold(ChessGame(players = players, dateTime = date))(fen =>
              ChessGame.copy(FenParser.fromFen(fen))(newPlayers = players, newDateTime = date)
            )
          ) match {
            case scala.util.Success(game) =>
              moves.foldLeft[ValidationNel[String, ChessGame]](game.successNel) {(currentGame, move) =>
                currentGame.fold[ValidationNel[String, ChessGame]](
                  fail => s"An error occurred during a move: ${fail.head}".failureNel[ChessGame],
                  game => {
                    val res = PgnMoveParser(move.san, game)
                    observer.foreach(o => o.moveParsed(move.san))
                    res
                  }
                )
              }
            case scala.util.Failure(e) => s"An error occurred during the game creation: ${e.getMessage}".failureNel
          }
        })
      } else {
        "The specified PGN tags are not sufficient according to the Seven Tag Roster".failureNel
      }
    })
    parsedGame.fold[ChessGame](fail => throw new IllegalArgumentException(fail.head), game => game)

  }

}