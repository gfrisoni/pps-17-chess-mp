package it.unibo.chess.model.pgn

import it.unibo.chess.model.pgn.PgnTagTypes._

import scalaz.ValidationNel
import scalaz.Scalaz._

/**
  * Represents a generic tag inside a PGN.
  */
sealed trait PgnTag {

  /**
    * The type of the PGN tag.
    */
  val tagType: PgnTagType

  /**
    * The value associated to the PGN tag's type.
    */
  val value: String

}

/**
  * Handles the factories for a PGN tag.
  */
object PgnTag {

  /**
    * Creates a new PGN tag.
    *
    * @param tagType the type of the PGN tag
    * @param value the value associated to the PGN tag's type
    * @return the PGN tag if the optional format related to {{{tagType}}} is satisfied, a failure otherwise.
    */
  def apply(tagType: PgnTagType, value: Any): ValidationNel[String, PgnTag] = {
    val tagValue: String = value.toString
    tagType.format.fold[ValidationNel[String, PgnTag]](PgnTagImpl(tagType, tagValue).successNel)(f =>
      if (f(tagValue)) {
        PgnTagImpl(tagType, tagValue).successNel
      } else {
        s"The tag value '$tagValue' does not satisfy the format required by '${tagType.name}' type".failureNel
      }
    )
  }

  /**
    * Creates a new PGN tag.
    *
    * @param name the name of the PGN tag's type
    * @param value the value associated to the PGN tag's type
    * @return the PGN tag if the optional format related to {{{tagType}}} is satisfied, a failure otherwise.
    */
  def apply(name: String, value: Any): ValidationNel[String, PgnTag] = {
    apply(getPgnTagTypeFromName(name), value)
  }

  private case class PgnTagImpl(override val tagType: PgnTagType, override val value: String) extends PgnTag {
    override def toString: String = s"""[$tagType "$value"]"""
  }

}