package it.unibo.chess.model.pgn

import it.unibo.chess.model.core._
import it.unibo.chess.model.core.ChessRoles.{ChessRole, Pawn}
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.utilities.CharFormatUtilities._

import scala.collection.immutable.NumericRange
import scala.util.Try
import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers
import scalaz.ValidationNel
import scalaz.Scalaz._

/**
  * This object handles a regex parser for a PGN move.
  * It provides the ability of convert a string chess move expressed in Standard Algebraic Notation
  * into the chess game resulting from its application, starting from the specified one.
  */
object PgnMoveParser extends RegexParsers {

  override def skipWhitespace: Boolean = false

  private val fileRange: NumericRange.Inclusive[Char] = 'a' to 'h'
  private val rankRange: NumericRange.Inclusive[Char] = '1' to '8'

  private val moveRegex: Regex = """^(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)$""".r

  /**
    * Parses the specified move and creates the chess game resulting from its application.
    *
    * @param san the move expressed in Standard Algebraic Notation
    * @param game the starting game
    * @return the next chess game if the parsing is successful, a failure otherwise.
    */
  def apply(san: String, game: ChessGame): ValidationNel[String, ChessGame] = {
    val res: ValidationNel[String, Try[ChessGame]] = san match {
      case "0-0" => game.currentTurn match {
        case White => Try(game.move('e1, 'g1)).successNel
        case Black => Try(game.move('e8, 'g8)).successNel
      }
      case "0-0-0" => game.currentTurn match {
        case White => Try(game.move('e1, 'c1)).successNel
        case Black => Try(game.move('e8, 'c8)).successNel
      }
      case moveRegex(role, file, rank, capture, position, promotion, check, mate) =>
        val moveRes: ValidationNel[String, Try[ChessGame]] = role
          .headOption
          .fold[Option[ChessRole]](Some(Pawn(game.currentTurn)))(AnUtilities.getRole(_, game.currentTurn))
          .fold[ValidationNel[String, Try[ChessGame]]](s"The char $role does not match with any chess role".failureNel)
          { role =>
            position match {
              case pos if pos.nonEmpty =>
                Try(ChessBoardPosition(pos)) match {
                  case scala.util.Success(targetPosition) =>
                    if (!fileRange.contains(file)) s"The file $file is invalid".failureNel
                    if (!rankRange.contains(rank)) s"The rank $rank is invalid".failureNel
                    val sources: Map[BoardPosition, ChessPiece] = game.sourcesOf(
                      role,
                      game.currentTurn,
                      targetPosition,
                      (file, rank) match {
                        case (f, r) if f.nonEmpty && r.nonEmpty => Some(b => b.col == letterToInt(f) && b.row == r.charAt(0).asDigit)
                        case (f, _) if f.nonEmpty => Some(b => b.col == letterToInt(f))
                        case (_, r) if r.nonEmpty => Some(b => b.row == r.charAt(0).asDigit)
                        case _ => None
                      }
                    )
                    val newGame: ValidationNel[String, Try[ChessGame]] = sources.size match {
                      case s if s > 1 => s"The move $san is ambiguous: multiple pieces can go into $position square".failureNel
                      case s if s <= 0 => s"The move $san is illegal for the chess role $role".failureNel
                      case _ =>
                        role match {
                          case Pawn(_) =>
                            promotion match {
                              case prom if prom.nonEmpty =>
                                AnUtilities.getRole(prom.last, game.currentTurn)
                                  .fold[ValidationNel[String, Try[ChessGame]]](s"The char $role used as piece promotion does not match with any chess role".failureNel)
                                  { newRole => Try(game.move(sources.head._1, targetPosition).promote(targetPosition, newRole)).successNel }
                              case _ => Try(game.move(sources.head._1, targetPosition)).successNel
                            }
                          case r =>
                            promotion match {
                              case prom if prom.nonEmpty => s"A piece with role $r cannot promote".failureNel
                              case _ => Try(game.move(sources.head._1, targetPosition)).successNel
                            }
                        }
                    }
                    newGame
                  case scala.util.Failure(e) => s"Invalid target position: ${e.getMessage}".failureNel
                }
              case _ => "The SAN string contains an empty target position".failureNel
            }
          }
        // Checks for san - resulting game action inconsistencies
        moveRes.fold[ValidationNel[String, Try[ChessGame]]](fail => fail.head.failureNel, { _.filter(game => {
         game.history.gameSteps.last.action match {
           case action if action.isInstanceOf[CheckableAction] => action.asInstanceOf[CheckableAction].kingState match {
             case KingStates.Free if check.nonEmpty || mate.nonEmpty => false
             case KingStates.Check if check.isEmpty || mate.nonEmpty => false
             case _ => action match {
               case validAction if validAction.isInstanceOf[ChessCaptureAction] && capture.isEmpty => false
               case validAction if (!validAction.isInstanceOf[ChessCaptureAction] && !validAction.isInstanceOf[EnPassantAction]) && capture.nonEmpty => false
               case validAction if !validAction.isInstanceOf[ChessPromotionAction] && promotion.nonEmpty => false
               case _ => true
             }
           }
           case _ => true
         }
        }).successNel})
      case _ => s"The SAN string $san has an invalid syntax".failureNel
    }
    res.fold[ValidationNel[String, ChessGame]](fail => fail.head.failureNel, {
      case scala.util.Success(newGame) => newGame.successNel
      case scala.util.Failure(e) => e.getMessage.failureNel
    })
  }

}