package it.unibo.chess.model.logic

import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.utilities.ChessUtilities.EnPassantUtilities._

import scala.language.reflectiveCalls

/**
  * Side effects applicable into some particular cases.
  */
object SideEffects {

  /**
    * The main trait for a side effect.
    */
  sealed trait ChessPieceSideEffect {

    /**
      * Involved piece.
      */
    val piece: ChessPiece

    /**
      * Effect applied before moving.
      *
      * @param board the starting board
      * @param target the target where to apply the effect
      * @return the new board once the effect is applied.
      */
    def applyEffect(board: ChessBoard, target: BoardPosition): ChessBoard
  }

  /**
    * Side effect to perform enPassant.
    *
    * @param piece the Pawn doing the move
    */
  case class EnPassantEffect(override val piece: ChessPiece) extends ChessPieceSideEffect {
    override def applyEffect(board: ChessBoard, target: BoardPosition): ChessBoard = {
      if((board pieceAt target).isDefined) board
      else board take getOpponentPosition(piece.color, target)
    }
  }

  /**
    * Side effect to perform the Castling.
    *
    * @param piece the King doing Castling
    */
  case class CastlingEffect(override val piece: ChessPiece) extends ChessPieceSideEffect {
    override def applyEffect(board: ChessBoard, target: BoardPosition): ChessBoard = {
      val kingPosition = piece.color.castling.kingPosition
      val (rookFrom: BoardPosition, rookTo: BoardPosition) =
        if (target.col < kingPosition.col) (piece.color.castling.leftRookPosition, piece.color.castling.leftRookDestination)
        else (piece.color.castling.rightRookPosition, piece.color.castling.rightRookDestination)
      board move rookFrom to rookTo
    }
  }

}