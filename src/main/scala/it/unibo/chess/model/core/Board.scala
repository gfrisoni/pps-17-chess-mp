package it.unibo.chess.model.core

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Board.Types.{NoPiece, OccupiedPosition}
import it.unibo.chess.exceptions.Promotion.PromotionException
import it.unibo.chess.exceptions.Promotion.Types.{NoPawnOnPosition, NotAPawn, NotOnPromotionLine, WrongRole}
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.utilities.ChessUtilities.AnUtilities._
import it.unibo.chess.model.utilities.ChessUtilities._

/**
  * Represents a general board for every board-based game.
  *
  * @tparam P The type of pieces that will populate the board
  */
sealed trait Board[P <: Piece[_]] {

  /**
    * The pieces on the board, identified by their position.
    */
  val pieces: Map[BoardPosition, P]

  /**
    * The taken pieces.
    */
  val taken: List[P]

  /**
    * Puts a piece on a given destination.
    *
    * @param piece the piece to put
    * @return the result of {{{at}}} method, the new board.
    */
  def put(piece: P): {

    // The destination of the put call
    def at(destination: BoardPosition): Board[P]

  }

  /**
    * Resets the board on the Chess standard positions with all the pieces on it.
    *
    * @return the reset board.
    */
  def reset(): Board[P]

  /**
    * Removes an existing piece from the board and considers it as "taken".
    *
    * @param from the occupied position of the piece to be taken
    * @return the new board without the piece in {{{from}}} position.
    */
  def take(from: BoardPosition): Board[P]

  /**
    * Obtains the piece on a given position.
    *
    * @param position the position where to look about the piece
    * @return the chess piece if found, {{{None}}} if empty.
    */
  def pieceAt(position: BoardPosition): Option[P]

  /**
    * Moves a piece on a destination.
    *
    * @param origin the position with the piece to move
    * @return the new board after moving the piece.
    */
  def move(origin: BoardPosition): {

    // The destination of the move action
    def to(destination: BoardPosition): Board[P]

  }

}

/**
  * The representation of a board used on Chess.
  *
  * @param pieces the alive pieces with their position on the board
  * @param taken the taken pieces
  */
case class ChessBoard(
                       override val pieces: Map[BoardPosition, ChessPiece] = Map.empty,
                       override val taken: List[ChessPiece] = List.empty
                     ) extends Board[ChessPiece] {

  /**
    * The position of the kings mapped by color.
    */
  lazy val kingPos: Map[ChessColor, BoardPosition] = pieces.collect {
    case (pos, ChessPiece(_, King(color))) => color -> pos
  }

  /**
    * The position of a king by a given color.
    *
    * @param color the chess color used for the research
    * @return the position occupied by the king with {{{color}}}.
    */
  def kingPosOf(color: ChessColor): BoardPosition = kingPos(color)

  /**
    * Gets all the alive pieces of a given color.
    *
    * @param color the chess color used for the research
    * @return the pieces filtered.
    */
  def piecesOf(color: ChessColor): Map[BoardPosition, ChessPiece] = pieces.filter(_._2.color == color)

  /**
    * Gets all the alive pieces by a given role.
    *
    * @param role the chess role used for the research
    * @return the pieces filtered.
    */
  def piecesOf(role: ChessRole): Map[BoardPosition, ChessPiece] = pieces.filter(_._2.role == role)

  override def put(piece: ChessPiece): {
    def at(destination: BoardPosition): ChessBoard
  } = {
    case class PutAt() {
      def at(destination: BoardPosition): ChessBoard = ChessBoard(pieces + (destination -> piece), taken)
    }
    new PutAt
  }

  override def reset(): ChessBoard = {
    val pairs = for  (col <- sideLengthIterable;
                      row <- List(White.startingRow, White.pawnLine, Black.pawnLine, Black.startingRow)) yield (ChessBoardPosition(col, row),
      row match {
        case White.startingRow => ChessPiece(White, defaultChessPattern(White)(col - 1))
        case White.pawnLine => ChessPiece(White, Pawn(White))
        case Black.pawnLine => ChessPiece(Black, Pawn(Black))
        case Black.startingRow => ChessPiece(Black, defaultChessPattern(Black)(col - 1))
      })
    ChessBoard(pairs.foldLeft(Map.empty[BoardPosition, ChessPiece]) {(acc, next) => acc + (next._1 -> next._2)}, Nil)
  }

  override def take(from: BoardPosition): ChessBoard = ChessBoard(pieces - from, pieces(from) :: taken)

  override def pieceAt(position: BoardPosition): Option[ChessPiece] = Option(pieces.getOrElse(position, null))

  override def move(origin: BoardPosition): {
    def to(destination: BoardPosition): ChessBoard
  } = {
    case class To() {
      def to(destination: BoardPosition): ChessBoard = {
        if (pieces contains destination) throw BoardException(s"Cannot move to occupied $destination", OccupiedPosition)
        else pieces.get(origin).map(piece => ChessBoard((pieces - origin) + (destination -> piece), taken))
          .getOrElse {throw BoardException(s"No piece at $origin to move", NoPiece)}
      }
    }
    new To
  }

  /**
    * Promotes a pawn on the board.
    *
    * @param position the position where the pawn to be promoted is
    * @return the new board with the pawn promoted into the given role.
    */
  def promote(position: BoardPosition): {
    // The new role the pawn should be promoted into
    def to(role: ChessRole): ChessBoard
  } = {
    case class Promotion() {
      def to(role: ChessRole): ChessBoard = role match {
        case King(_) => throw PromotionException("Can't promote to King", WrongRole)
        case _ => pieces.get(position).map(piece => piece.role match {
          case Pawn(piece.color) if position.row == piece.color.promotionLine => ChessBoard(pieces + (position -> ChessPiece(piece.color, role)), taken)
          case Pawn(piece.color) => throw PromotionException(s"A ${piece.color} Pawn can only be promoted on line ${piece.color.promotionLine}", NotOnPromotionLine)
          case _ => throw PromotionException("Only Pawns can be promoted", NotAPawn)
        }).getOrElse {throw PromotionException(s"No Piece at $position to Promote", NoPawnOnPosition)}
      }
    }
    new Promotion
  }

  override def toString: String = {
    (for (row <- sideLengthIterable.reverse; col <- sideLengthIterable) yield ChessBoardPosition(col,row)).foldLeft("")(
      (acc, pos) => {
        val piece: Option[ChessPiece] = pieces.get(pos)
        val pieceString = piece match {
          case Some(p) => figurine(p.color, p.role)
          case None => "▭"
        }
        acc +
        (if (pos.col == BOARD_START_INDEX) pos.row + " " else "") +
        pieceString +
        (if (pos.col == BOARD_END_INDEX) "\n" else "")
      }
    )
  }

}