package it.unibo.chess.model.core

/**
  * Represents the player of a generic game.
  */
sealed trait Player {

  /**
    * The name of the player.
    */
  val name: String

}

/**
  * Models the player of a chess game.
  *
  * @param name the name of the player
  */
case class ChessPlayer(override val name: String = "unknown") extends Player