package it.unibo.chess.model.core.position

import it.unibo.chess.model.utilities.ChessUtilities

import scala.language.implicitConversions

/**
  * Handles the utilities for a chess board position.
  * It defines factories and advanced moves.
  */
object ChessBoardPosition {

  /**
    * Creates a new chess board position.
    *
    * @param s the string to convert into the chess board position
    * @return the chess board position related to {{{s}}}.
    */
  def apply(s: String): BoardPosition = apply(Symbol(s))

  /**
    * Creates a new chess board position.
    *
    * @param s the symbol to convert into the chess board position
    * @return the chess board position related to {{{s}}}.
    */
  def apply(s: Symbol): BoardPosition = apply(ChessUtilities.coordinateToTuple(s))

  /**
    * Creates a new chess board position.
    *
    * @param colAndRow the tuple of coordinates to convert into the chess board position
    * @return the chess board position related to {{{colAndRow}}}.
    */
  def apply(colAndRow: (Int, Int)): BoardPosition = new BoardPosition(colAndRow._1, colAndRow._2)(ChessUtilities.sideLengthIterable)


  /**
    * Calculates a collection of chess board positions from a collection of strings.
    *
    * @param positions the string positions to convert
    * @return the collection of chess board positions resulting from the conversion.
    */
  def getPositionsFromStrings(positions: Iterable[String]): Iterable[BoardPosition] = positions.map(pos => this.apply(pos))

  /**
    * Calculates a collection of chess board positions from a collection of symbols.
    *
    * @param positions the symbol positions to convert
    * @return the collection of chess board positions resulting from the conversion.
    */
  def getPositionsFromSymbols(positions: Iterable[Symbol]): Iterable[BoardPosition] = positions.map(pos => this.apply(pos))

  /**
    * Calculates a collection of chess board positions from a collection of coordinates pairs.
    *
    * @param positions the coordinates pairs to convert
    * @return the collection of chess board positions resulting from the conversion.
    */
  def getPositions(positions: Iterable[(Int, Int)]): Iterable[BoardPosition] = positions.map(pos => this.apply(pos))


  /**
    * Calculates the chess board position placed above a specified one.
    *
    * @param p the starting chess board position
    * @return the chess board position placed above {{{p}}}.
    */
  def ^(p: Option[BoardPosition]): Option[BoardPosition] = p.flatMap(_ ^ 1)

  /**
    * Calculates the chess board position placed on the right of a specified one.
    *
    * @param p the starting chess board position
    * @return the chess board position placed on the right of {{{p}}}.
    */
  def >(p: Option[BoardPosition]): Option[BoardPosition] = p.flatMap(_ > 1)

  /**
    * Calculates the chess board position placed below a specified one.
    *
    * @param p the starting chess board position
    * @return the chess board position placed below {{{p}}}.
    */
  def v(p: Option[BoardPosition]): Option[BoardPosition] = p.flatMap(_ v 1)

  /**
    * Calculates the chess board position placed on the left of a specified one.
    *
    * @param p the starting chess board position
    * @return the chess board position placed on the left of {{{p}}}.
    */
  def <(p: Option[BoardPosition]): Option[BoardPosition] = p.flatMap(_ < 1)

  /**
    * Calculates the chess board positions that are reachable from a starting one with a vector movement,
    * according to specified directions.
    *
    * @param from the starting chess board position
    * @param directions the directions to consider
    * @return the encountered chess board positions on each direction.
    */
  def vectorBasedPositions(from: BoardPosition, directions: List[List[Option[BoardPosition] => Option[BoardPosition]]]): List[List[BoardPosition]] = {
    def expand(direction: Seq[Option[BoardPosition] => Option[BoardPosition]]): List[BoardPosition] = {
      def next(accumulator: List[BoardPosition]): List[BoardPosition] = {
        val seed: Option[BoardPosition] = Some(accumulator.headOption.getOrElse(from))
        val candidate = direction.foldLeft(seed) {(intermediate, step) => step(intermediate)}
        candidate match {
          case Some(position) => next(position :: accumulator)
          case None => accumulator
        }
      }
      next(Nil).reverse
    }
    directions.foldLeft(Nil: List[List[BoardPosition]]) {(accumulator, next) => expand(next) :: accumulator}.filter(l => l.nonEmpty).reverse
  }

  /**
    * Calculates the chess board positions that are reachable from a starting one with a radial movement,
    * according to specified offsets and a filtering logic.
    *
    * @param from the starting chess board position
    * @param offsets the offsets to consider for the the radial movement
    * @param filter the filtering logic to use
    * @return the encountered chess board positions.
    */
  def radialBasedPositions(from: BoardPosition, offsets: Iterable[Int], filter: (Int, Int) => Boolean): Iterable[BoardPosition] = {
    (for (row <- offsets; col <- offsets; if filter(row, col)) yield (from ^ row).flatMap(_ < col)).filter(_.isDefined).map(_.get)
  }

  /**
    * Handles a useful implicit in order to auto-convert a symbol into a chess board position.
    */
  object SymbolConverter {
    implicit def toPosition(symbol: Symbol): BoardPosition = ChessBoardPosition(symbol)
  }

}