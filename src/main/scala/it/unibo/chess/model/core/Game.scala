package it.unibo.chess.model.core

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Board.Types.NoPiece
import it.unibo.chess.exceptions.Game.GameException
import it.unibo.chess.exceptions.Game.Types.{IllegalMove, InvalidPlayersSetup, PendingPromotion, WrongTurn}
import it.unibo.chess.exceptions.Promotion.PromotionException
import it.unibo.chess.exceptions.Promotion.Types.EmptyPromotionLine
import it.unibo.chess.model.history.ChessActions._
import it.unibo.chess.model.logic.PieceQueries._
import it.unibo.chess.model.logic.SideEffects.{CastlingEffect, ChessPieceSideEffect, EnPassantEffect}
import it.unibo.chess.model.core.ChessRoles.{ChessRole, King, Pawn}
import it.unibo.chess.model.core.KingStates._
import it.unibo.chess.model.logic.{Continue, IncludeAndStop, Stop}
import it.unibo.chess.model.core.AmbiguityTypes.AmbiguityType
import it.unibo.chess.model.core.DrawReasons.DrawReason
import it.unibo.chess.model.core.LoseReasons.LoseReason
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.utilities.ChessUtilities.EnPassantUtilities
import com.github.nscala_time.time.Imports._
import it.unibo.chess.model.history.{BoardGameHistory, ChessGameHistory, ChessGameStep}

import scala.language.reflectiveCalls

/**
  * Represents a generic board-based game.
  *
  * @tparam Game the specific type of game to consider
  */
sealed trait BoardGame[Game <: BoardGame[Game]] {

  /**
    * The type of the pieces to use during the game.
    */
  type GamePiece <: Piece[_]

  /**
    * The type of the board on which play.
    */
  type GameBoard <: Board[GamePiece]

  /**
    * The history format to use for game actions' storing.
    */
  type GameHistory <: BoardGameHistory[GamePiece, _, _, _]

  /**
    * The type of the player.
    */
  type GamePlayer <: Player

  /**
    * The type of the identifier to use with a player.
    */
  type PlayerId

  /**
    * The board of the game.
    */
  val board: GameBoard

  /**
    * The players of the game.
    */
  val players: Map[PlayerId, GamePlayer]

  /**
    * The identifier of the turn owner.
    */
  val currentTurn: PlayerId

  /**
    * The history of the game.
    */
  val history: GameHistory

  /**
    * Moves a piece.
    *
    * @param startingPosition the starting position of the piece
    * @param finalPosition the desired destination of the piece
    * @return the new game with the move performed.
    */
  def move(startingPosition: BoardPosition, finalPosition: BoardPosition): Game

  /**
    * Sets the game to draw.
    *
    * @param drawReason the reason for the GameHistory
    * @return the new game with the draw action added.
    */
  def draw(drawReason: DrawReason): Game

  /**
    * Sets the game to over, with a loser and a winner.
    *
    * @param loser the player that lost the game
    * @param loseReason the reason for the GameHistory
    * @return the ended game.
    */
  def lose(loser: PlayerId, loseReason: LoseReason): Game

}

/**
  * A specific type of board game for Chess.
  */
sealed trait ChessGame extends BoardGame[ChessGame] {

  override type GamePiece = ChessPiece
  override type GameBoard = ChessBoard
  override type GameHistory = ChessGameHistory
  override type GamePlayer = ChessPlayer
  override type PlayerId = ChessColor

  /**
    * The utility flags used for the chess game.
    */
  val flags: ChessFlags

  /**
    * The date on which the game takes place.
    */
  val dateTime: DateTime

  /**
    * Determines the state of each king.
    *
    * @param victim the color of the king
    * @return the state of the specified king.
    */
  def kingStateOf(victim: ChessColor): KingState

  /**
    * Calculates all the possible destinations from a given position occupied by a piece
    *
    * @param startingPosition the starting position
    * @param simulateNext flag to decide if a simulation on the next move is necessary (used to identify check moves)
    * @return the set of all the allowed destinations.
    */
  def movesFrom(startingPosition: BoardPosition, simulateNext: Boolean = true): Set[BoardPosition]

  /**
    * Checks whether it is possible to claim a draw for the 50rule.
    *
    * @return {{{true}}} or {{{false}}} if the rule is applicable.
    */
  def isFiftyRuleApplicable: Boolean

  /**
    * Promotes a pawn according to the board rules.
    *
    * @param position the position of the pawn to be promoted
    * @param newRole the new role that will be given to the pawn
    * @return the new game with the promoted pawn.
    */
  def promote(position: BoardPosition, newRole: ChessRole): ChessGame

  /**
    * Gets all the pieces with the specified role and color that can move to a certain target position,
    * with an optional filtering logic.
    *
    * @param role the chess role to consider
    * @param color the chess color to consider
    * @param targetPosition the destination
    * @param filter the optional filtering logic to apply during pieces research
    * @return the pieces that can move to {{{targetPosition}}}.
    */
  def sourcesOf(role: ChessRole,
                color: ChessColor,
                targetPosition: BoardPosition,
                filter: Option[BoardPosition => Boolean] = None): Map[BoardPosition, ChessPiece]

}

/**
  * Handles a chess game, providing factories and utilities.
  */
object ChessGame {

  /**
    * Creates a new chess game.
    *
    * @param board the board of the game
    * @param players players of the game
    * @param currentTurn the owner of the turn
    * @param history the history related to the game
    * @param flags the utility flags of the game
    * @param dateTime the date on which the game takes place
    * @return the new chess game.
    */
  def apply(board: ChessBoard = ChessBoard().reset(),
            players: Map[ChessColor, ChessPlayer] = Map.empty,
            currentTurn: ChessColor = White,
            history: ChessGameHistory = ChessGameHistory(),
            flags: ChessFlags = ChessFlags(),
            dateTime: DateTime = DateTime.now()): ChessGame = ChessGameImpl(board, players, currentTurn, history, flags, dateTime)

  /**
    * Creates a new chess game from an existing one.
    *
    * @param game the original game to copy
    * @param newBoard the board of the new game
    * @param newPlayers the players of the new game
    * @param newCurrentTurn the owner of the turn inside the new game
    * @param newHistory the history related to the new game
    * @param newFlags the utility flags of the new game
    * @param newDateTime the date on which the new game takes place
    * @return the new chess game.
    */
  def copy(game: ChessGame)
          (newBoard: ChessBoard = game.board,
           newPlayers: Map[ChessColor, ChessPlayer] = game.players,
           newCurrentTurn: ChessColor = game.currentTurn,
           newHistory: ChessGameHistory = game.history,
           newFlags: ChessFlags = game.flags,
           newDateTime: DateTime = game.dateTime): ChessGame = {
    ChessGameImpl(newBoard, newPlayers, newCurrentTurn, newHistory, newFlags, newDateTime)
  }

  private case class ChessGameImpl(
                                    override val board: ChessBoard = ChessBoard().reset(),
                                    override val players: Map[ChessColor, ChessPlayer] = Map.empty,
                                    override val currentTurn: ChessColor = White,
                                    override val history: ChessGameHistory = ChessGameHistory(),
                                    override val flags: ChessFlags = ChessFlags(),
                                    override val dateTime: DateTime = DateTime.now()
                                  ) extends ChessGame {

    require(players.size == 2, throw GameException("Incorrect number of players", InvalidPlayersSetup))
    require(players.contains(White) && players.contains(Black), throw GameException("There must be a white and a black player", InvalidPlayersSetup))

    private var legalMovesMap: Map[BoardPosition, List[(BoardPosition, Option[ChessPieceSideEffect])]] = Map.empty

    private def isCheckOf(victim: ChessColor): Boolean = {
      val kingPos: BoardPosition = board.kingPosOf(victim)
      board.piecesOf(!victim).keys.exists(p => ChessGame(board,players,currentTurn).movesFrom(p, simulateNext = false).contains(kingPos))
    }

    override def kingStateOf(victim: ChessColor): KingState = {
      val check: Boolean = isCheckOf(victim)
      if (board.piecesOf(victim).keys.forall(p => movesFrom(p).isEmpty)) {
        if (check) Checkmate else Stalemate
      } else {
        if (check) Check else Free
      }
    }

    override def movesFrom(startingPosition: BoardPosition, simulateNext: Boolean = true): Set[BoardPosition] = {
      def follow(direction: List[BoardPosition], queryAndEffect: (ChessPieceQuery, Option[ChessPieceSideEffect])): Set[BoardPosition] = {
        def next(allowedPositions: Set[BoardPosition], nextPositions: List[BoardPosition]): Set[BoardPosition] = {
          nextPositions match {
            case _ :: _ =>
              val destination: BoardPosition = nextPositions.head
              if (legalMovesMap.isDefinedAt(startingPosition)) {
                legalMovesMap = legalMovesMap + (startingPosition -> ((destination, queryAndEffect._2) :: legalMovesMap(startingPosition)))
              } else {
                legalMovesMap = legalMovesMap + (startingPosition -> List((destination, queryAndEffect._2)))
              }
              queryAndEffect._1.applyQuery(this, destination) match {
                case Continue => next(allowedPositions + destination, nextPositions.tail)
                case IncludeAndStop => allowedPositions + destination
                case Stop => allowedPositions
              }
            case _ => allowedPositions
          }
        }

        if (simulateNext) {
          next(Set.empty, direction)
            .filter(p => {
              val simulatedGame: ChessGameImpl = this.copy(
                board = (board pieceAt p).fold(board)(_ => board take p) move startingPosition to p,
                currentTurn = !currentTurn
              )
              !(simulatedGame.board.taken.contains(ChessPiece(currentTurn, King(currentTurn))) || simulatedGame.isCheckOf(currentTurn))
            })
        } else {
          next(Set.empty, direction)
        }
      }

      board.pieces.get(startingPosition) match {
        case None => throw BoardException(s"There isn't any piece at $startingPosition", NoPiece)
        case Some(piece) =>
          if (legalMovesMap.contains(startingPosition)) {
            legalMovesMap(startingPosition).map(_._1).toSet
          } else {
            val allowedPositions = piece.validatedDirectionsFrom(startingPosition).foldLeft(Set.empty[BoardPosition]) {
              (acc, directionAndQuery) => acc ++ follow(directionAndQuery._1, directionAndQuery._2)
            }
            legalMovesMap = legalMovesMap + (startingPosition -> legalMovesMap.getOrElse(startingPosition, List.empty).filter(p => allowedPositions.contains(p._1)))
            allowedPositions
          }
      }
    }

    override def sourcesOf(role: ChessRole,
                           color: ChessColor,
                           targetPosition: BoardPosition,
                           sourcesFilter: Option[BoardPosition => Boolean]): Map[BoardPosition, ChessPiece] = {
      board.piecesOf(role).filter(p => p._2.color == color &&
        sourcesFilter.fold(true)(f => f(p._1)) &&
        movesFrom(p._1).contains(targetPosition))
    }

    override def move(startingPosition: BoardPosition, finalPosition: BoardPosition): ChessGame = {
      if (flags.promotionAvailability) throw GameException("Cannot move with a pending promotion", PendingPromotion)
      val opponent = !currentTurn
      val fromPiece = board pieceAt startingPosition
      val toPiece = board pieceAt finalPosition
      if (fromPiece.exists(_.color == !currentTurn)) throw GameException("Cannot move the specified piece because of wrong turn!", WrongTurn)
      if (!legalMovesMap.isDefinedAt(startingPosition)) movesFrom(startingPosition)
      legalMovesMap(startingPosition).find(_._1 == finalPosition).fold(throw GameException(s"Invalid destination from $startingPosition to $finalPosition", IllegalMove))(
        legalMove => {
          // Calculates all the possible sources for the final position and selects the ambiguity type
          val ambiguity: AmbiguityType = sourcesOf(
            fromPiece.get.role,
            fromPiece.get.color,
            finalPosition,
            fromPiece.get.role match {
              case Pawn(_) => Some(b => (startingPosition > ((finalPosition.col - startingPosition.col) * 2)).fold(false)(_ == b))
              case _ => None
            }
          ) - startingPosition match {
            case s if s.isEmpty => AmbiguityTypes.None
            case s if s.forall(p => p._1 |? startingPosition) => AmbiguityTypes.SameFileAmbiguity
            case s if s.forall(p => p._1 -? startingPosition) => AmbiguityTypes.SameRankAmbiguity
            case _ => AmbiguityTypes.CompleteAmbiguity
          }
          var appliedSideEffect: Option[ChessPieceSideEffect] = None
          val sideEffectBoard = legalMove._2.fold(board)(
            sideEffect => {
              appliedSideEffect = Some(sideEffect)
              sideEffect.applyEffect(board, finalPosition)
            }
          )
          if (sideEffectBoard == board) appliedSideEffect = None
          val (newBoard, tmpAction): (ChessBoard, ChessAction) = if (toPiece.exists(_.color == opponent)) {
            ((sideEffectBoard take finalPosition) move startingPosition to finalPosition,
              CaptureAction((fromPiece.get, startingPosition), ambiguity, finalPosition, toPiece.get))
          } else {
            (sideEffectBoard move startingPosition to finalPosition,
              MoveAction((fromPiece.get, startingPosition), ambiguity, finalPosition))
          }
          val newFlags: ChessFlags = ChessFlagsUtilities.updateFlags(fromPiece.get, startingPosition, finalPosition, tmpAction, flags)
          val newAction: ChessAction = appliedSideEffect.fold(tmpAction){
            case EnPassantEffect(_) => EnPassantAction(
              (fromPiece.get, startingPosition),
              ambiguity,
              finalPosition,
              (board pieceAt EnPassantUtilities.getOpponentPosition(currentTurn, finalPosition)).get
            )
            case CastlingEffect(_) => CastlingAction(CastlingConstants(currentTurn).isKingCastling(finalPosition.col))
          }
          newAction match {
            case action: CheckableAction =>
              val nextGame: ChessGameImpl = this.copy(board = newBoard, currentTurn = opponent)
              val nextKingState: KingState = nextGame.kingStateOf(opponent)
              val newActionWithCheck: ChessAction = action match {
                case a: MoveAction => a.copy(kingState = nextKingState)
                case a: CaptureAction => a.copy(kingState = nextKingState)
                case a: CapturePromoteAction => a.copy(kingState = nextKingState)
                case a: EnPassantAction => a.copy(kingState = nextKingState)
                case a: PromoteAction => a.copy(kingState = nextKingState)
                case a: CastlingAction => a.copy(kingState = nextKingState)
              }
              nextGame.copy(history = ChessGameHistory(history.gameSteps ++: Seq(ChessGameStep(newActionWithCheck, newBoard)) ++:
                { nextKingState match {
                  case Checkmate => Seq(ChessGameStep(LoseAction(opponent, LoseReasons.CheckmateLoss), newBoard))
                  case Stalemate => Seq(ChessGameStep(DrawAction(DrawReasons.StalemateDraw), newBoard))
                  case _ => Seq.empty
                }}), currentTurn = if (newFlags.promotionAvailability) currentTurn else opponent, flags = newFlags)
            case _ =>
              this.copy(
                board = newBoard,
                currentTurn = if (newFlags.promotionAvailability) currentTurn else opponent,
                history = ChessGameHistory(history.gameSteps :+ ChessGameStep(newAction, newBoard)),
                flags = newFlags
              )
          }
        }
      )
    }

    override def draw(drawReason: DrawReason): ChessGame = this.copy(history = ChessGameHistory(
      history.gameSteps :+ ChessGameStep(DrawAction(drawReason), board)
    ))

    override def lose(loser: ChessColor, loseReason: LoseReason): ChessGame = this.copy(history = ChessGameHistory(
      history.gameSteps :+ ChessGameStep(LoseAction(loser, loseReason), board)
    ))

    override def isFiftyRuleApplicable: Boolean = flags.halfMove >= 50

    override def promote(position: BoardPosition, newRole: ChessRole): ChessGame = {
      if (!flags.promotionAvailability) throw PromotionException("Cannot promote if there isn't any pawn on the allowed row", EmptyPromotionLine)
      val promotedBoard = board promote position to newRole
      val newKingState: KingState = this.copy(board = promotedBoard).kingStateOf(!currentTurn)
      def updateLastGameStep(history: ChessGameHistory, gameStep: ChessGameStep[ChessAction]): ChessGameHistory = {
        ChessGameHistory((history.gameSteps dropRight 1) :+ gameStep)
      }

      if (history.gameSteps.nonEmpty) {
        val newFlags = flags.copy(promotionAvailability = false)
        history.gameSteps.last.action match {
          case a: CaptureAction if a.toPosition == position =>
            this.copy(
              board = promotedBoard,
              currentTurn = !currentTurn,
              history = updateLastGameStep(history, ChessGameStep(CapturePromoteAction(a.from, a.ambiguity, a.toPosition, a.capturedPiece, newRole, newKingState), promotedBoard)),
              flags = newFlags
            )
          case a: MoveAction if a.toPosition == position =>
            this.copy(
              board = promotedBoard,
              currentTurn = !currentTurn,
              history = updateLastGameStep(history, ChessGameStep(PromoteAction(a.from, a.ambiguity, a.toPosition, newRole, newKingState), promotedBoard)),
              flags = newFlags
            )
          case _ => throw GameException("Cannot promote a pawn in a turn different from the one related to the movement", WrongTurn)
        }
      } else {
        throw GameException("Cannot promote a pawn in a turn different from the one related to the movement", WrongTurn)
      }
    }

    override def toString: String = {
      s"""Players: ${players.map{case (k, v) => s"${v.name} ($k)"}.mkString(", ")}
         |Moves:${flags.fullMove} Full - ${flags.halfMove} Half
         |Available Castlings: ${flags.castlingAvailability.filter(_._2).map(a => (a._1._1, a._1._2)).map{case (c, d) => s"$c on $d"}.mkString(", ")}
         |Promotion is${if (flags.promotionAvailability) " " else " not "}available
         |
         |Turn: ${currentTurn.toString} moves!
         |
         |${board.toString}""".stripMargin
    }

  }

}