package it.unibo.chess.model.core

import it.unibo.chess.model.core.position.ChessBoardPosition._
import it.unibo.chess.model.core.Vectors._
import it.unibo.chess.model.core.position.BoardPosition

/**
  * Defines movements on vector directions.
  */
object Vectors {

  private val straightDirections: List[List[Option[BoardPosition] => Option[BoardPosition]]] =
    List(List(^), List(>), List(v), List(<))
  private val diagonalDirections: List[List[Option[BoardPosition] => Option[BoardPosition]]] =
    List(List(^, <), List(^, >), List(v, <), List(v, >))
  private val multiDirections: List[List[Option[BoardPosition] => Option[BoardPosition]]] = straightDirections ::: diagonalDirections

  /**
    * Represents a composed vector.
    *
    * @param directions the directions to combine
    */
  sealed abstract class Vector(val directions: List[List[Option[BoardPosition] => Option[BoardPosition]]])

  /**
    * A straight vector composed by vertical and horizontal directions.
    */
  case object StraightVector extends Vector(straightDirections)

  /**
    * A diagonal vector.
    */
  case object DiagonalVector extends Vector(diagonalDirections)

  /**
    * A vector composed by straight and diagonal directions.
    */
  case object MultiVector extends Vector(multiDirections)

}

/**
  * A generic board-based role.
  */
sealed trait BoardRole {

  /**
    * Calculates the positions on the board following the possible directions.
    *
    * @param position the starting position from which expand
    * @return the allowed positions along directions.
    */
  def directionsFrom(position: BoardPosition): List[List[BoardPosition]]

}

/**
  * A board-based role with a vector movement.
  */
abstract class VectorBoardRole extends BoardRole {

  /**
    * @return the vector followed by the role.
    */
  def vector: Vector

  override def directionsFrom(position: BoardPosition): List[List[BoardPosition]] =
    vectorBasedPositions(position, vector.directions)

}

/**
  * A board-based role with a radial movement.
  */
abstract class RadialBoardRole extends BoardRole {

  /**
    * @return the offsets from which to move radially.
    */
  def offsets: Iterable[Int]

  /**
    * @return the filter to apply during the radial movement.
    */
  def filter: (Int, Int) => Boolean

  override def directionsFrom(position: BoardPosition): List[List[BoardPosition]] =
    radialBasedPositions(position, offsets, filter).foldLeft(List[List[BoardPosition]]())((acc, dir) => List(dir) :: acc)

}

/**
  * Handles the roles supported by a chess game.
  */
object ChessRoles {

  /**
    * Represents a generic chess role.
    */
  sealed trait ChessRole extends BoardRole

  /**
    * Represents a chess role with a behaviour based on color.
    */
  sealed trait ColoredChessRole extends ChessRole {
    def color: ChessColor
  }

  /**
    * Models a King role.
    *
    * @param color the color of the king
    */
  case class King(override val color: ChessColor) extends RadialBoardRole with ColoredChessRole {
    override def offsets: Iterable[Int] = -1 to 1
    override def filter: (Int, Int) => Boolean = (row: Int, col: Int) => { row != 0 || col != 0 }
    override def directionsFrom(position: BoardPosition): List[List[BoardPosition]] = super[RadialBoardRole].directionsFrom(position) ++
      (if (position == color.castling.kingPosition) color.castling.kingDestinations else Nil)
  }

  /**
    * Models a Queen role.
    */
  case object Queen extends VectorBoardRole with ChessRole {
    override def vector: Vector = Vectors.MultiVector
  }

  /**
    * Models a Rook role.
    */
  case object Rook extends VectorBoardRole with ChessRole {
    override def vector: Vector = Vectors.StraightVector
  }

  /**
    * Models a Bishop role.
    */
  case object Bishop extends VectorBoardRole with ChessRole {
    override def vector: Vector = Vectors.DiagonalVector
  }

  /**
    * Models a Knight role.
    */
  case object Knight extends RadialBoardRole with ChessRole {
    import Math.abs
    override def offsets: Iterable[Int] = Set(-2, -1, 1, 2)
    override def filter: (Int, Int) => Boolean = (row: Int, col: Int) => { abs(row) != abs(col) }
  }

  /**
    * Models a Pawn role.
    *
    * @param color the color of the pawn
    */
  case class Pawn(override val color: ChessColor) extends ChessRole with ColoredChessRole {
    override def directionsFrom(position: BoardPosition): List[List[BoardPosition]] = {
      color match {
        case White => getMoves(position.row == White.pawnLine, position.^)
        case Black => getMoves(position.row == Black.pawnLine, position.v)
      }
    }

    private def getMoves(unmoved: Boolean, fwd: Int => Option[BoardPosition]): List[List[BoardPosition]] = {
      val moves: List[List[Option[BoardPosition]]] = List(
        if (unmoved) List(fwd(1), fwd(2)) else List(fwd(1)),
        List(<(fwd(1))),
        List(>(fwd(1)))
      )
      moves.map(_.filter(_.isDefined).map(_.get)).filter(_.nonEmpty)
    }
  }

  /**
    * All the roles required by the pieces of a chess game.
    */
  val roles: Seq[ChessRole] = Seq(King(White), King(Black), Queen, Rook, Bishop, Knight, Pawn(White), Pawn(Black))

  /**
    * All the legal roles a pawn can be promoted into.
    */
  val promotableRoles: Seq[ChessRole] = Seq(Queen, Rook, Bishop, Knight)

}