package it.unibo.chess.model.core

import it.unibo.chess.model.history.ChessActions.{CaptureAction, ChessAction}
import it.unibo.chess.model.core.ChessRoles.{King, Pawn, Rook}
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.utilities.CharFormatUtilities

/**
  * Traits to define flags to consider to handle the game.
  */
sealed trait Flags {

  val castlingAvailability: Map[(ChessColor, BoardPosition), Boolean]
  val enPassantTarget: Option[BoardPosition]
  val halfMove: Int
  val fullMove: Int
  val promotionAvailability: Boolean

  def canCastle(currentTurn: ChessColor, pos: BoardPosition): Boolean

}

/**
  * Object that handles utilities methods connected to game flags.
  */
object ChessFlagsUtilities {

  /**
    * Updates correctly flags depending on input parameters.
    *
    * @param fromPiece the piece selected for moving
    * @param startingPosition the staring position of the move
    * @param finalPosition the move final and desidered potision
    * @param action the action performed with that move
    * @param oldFlags old game flags on which the new ones depend one
    * @return new game flags
    */
   def updateFlags(fromPiece : ChessPiece, startingPosition: BoardPosition, finalPosition: BoardPosition, action: ChessAction, oldFlags: ChessFlags): ChessFlags =
     ChessFlags(
       castlingAvailability = fromPiece match {
         case ChessPiece(_, King(_)) =>
           oldFlags.castlingAvailability.foldLeft(Map.empty[(ChessColor, BoardPosition), Boolean])
           {(acc, next) => acc + (if(next._1._1 == fromPiece.color)next._1 -> false else next)}
         case ChessPiece(_, Rook) if startingPosition == fromPiece.color.castling.leftRookPosition || startingPosition == fromPiece.color.castling.rightRookPosition =>
           oldFlags.castlingAvailability + ((fromPiece.color, ChessBoardPosition(
             CharFormatUtilities.letterToInt(if(startingPosition == fromPiece.color.castling.leftRookPosition)
               CastlingConstants(fromPiece.color).queenCastlingDestination
               else CastlingConstants(fromPiece.color).kingCastlingDestination), fromPiece.color.castling.row)) -> false)
         case _ => oldFlags.castlingAvailability
       },
       enPassantTarget = fromPiece match {
         case ChessPiece(_, Pawn(_)) if startingPosition.row == fromPiece.color.pawnLine &&
           (finalPosition.row == White.pawnLine + 2 || finalPosition.row == Black.pawnLine - 2) =>
           Some(ChessBoardPosition(finalPosition.col, finalPosition.row + (fromPiece.color match {
             case White => - 1
             case Black => + 1
           })))
         case _ => None
       },
       halfMove = (fromPiece, action) match {
         case (ChessPiece(_, Pawn(_)), _) => 0
         case (_, CaptureAction(_, _, _, _, _)) => 0
         case (_, _) => oldFlags.halfMove + 1
       },
       fullMove = fromPiece.color match {
         case Black => oldFlags.fullMove + 1
         case _ => oldFlags.fullMove
       },
       promotionAvailability = fromPiece.role match {
         case Pawn(_) if finalPosition.row == fromPiece.color.promotionLine => true
         case _ => false
       }
     )

  /**
    * Check whether it is possible to castle or not.
    *
    * @param bool that represents the possibility of castling or not
    * @return a map with the association of castling availability
    */
  def castlingByBoolean(bool: Boolean): Map[(ChessColor, BoardPosition), Boolean] = {
    (for (color <- List(White, Black); pos <- color.castling.kingDestinations.map(_.head)) yield (color, pos))
      .foldLeft(Map.empty[(ChessColor, BoardPosition), Boolean]){ (acc, next) => acc + ((next._1, next._2)-> bool)}
  }
}

/**
  * Case class to correctly represents ChessFlags.
  *
  * @param castlingAvailability the availability of castling
  * @param enPassantTarget the position where it id possible to castle
  * @param halfMove the number of half moves
  * @param fullMove the number of full moves
  * @param promotionAvailability the availability, in term of position, to do an en-passant
  */
case class ChessFlags (
                        override val castlingAvailability: Map[(ChessColor, BoardPosition), Boolean] =
                          ChessFlagsUtilities.castlingByBoolean(true),
                        override val enPassantTarget: Option[BoardPosition] = None,
                        override val halfMove: Int = 0,
                        override val fullMove: Int = 1,
                        override val promotionAvailability: Boolean = false
                      ) extends Flags {

  override def canCastle(currentTurn: ChessColor, pos: BoardPosition): Boolean = castlingAvailability((currentTurn, pos))

}
