package it.unibo.chess.model.core.position

import java.lang.Math.abs

/**
  * Represents a general position.
  * It uses template method pattern.
  *
  * @tparam P the specific type of position to work with
  */
trait Position[P <: Position[P]] {

  /**
    * @return the column coordinate.
    */
  def col: Int

  /**
    * @return the row coordinate.
    */
  def row: Int


  /**
    * Calculates an above [[Position]].
    *
    * @param n the number of moves to consider
    * @return the [[Position]] placed {{{n}}} units above, if present.
    */
  def ^(n: Int): Option[P]

  /**
    * Calculates a [[Position]] to the right.
    *
    * @param n the number of moves to consider
    * @return the [[Position]] placed {{{n}}} units to the right, if present.
    */
  def >(n: Int): Option[P]

  /**
    * Calculates a below [[Position]].
    *
    * @param n the number of moves to consider
    * @return the [[Position]] placed {{{n}}} units below, if present.
    */
  def v(n: Int): Option[P]

  /**
    * Calculates a [[Position]] to the left.
    *
    * @param n the number of moves to consider
    * @return the [[Position]] placed {{{n}}} units to the left, if present.
    */
  def <(n: Int): Option[P]


  /**
    * Checks if another [[Position]] is on the same row.
    *
    * @param other the secondary [[Position]] to consider
    * @return {{{true}}} if the two positions are on the same row, {{{false}}} otherwise.
    */
  def -?(other: P): Boolean = row == other.row

  /**
    * Checks if another [[Position]] is on the same column.
    *
    * @param other the secondary [[Position]] to consider
    * @return {{{true}}} if the two positions are on the same column, {{{false}}} otherwise.
    */
  def |?(other: P): Boolean = col == other.col

  /**
    * Checks if another [[Position]] is on the same diagonal.
    *
    * @param other the secondary [[Position]] to consider
    * @return {{{true}}} if the two positions are on the same diagonal, {{{false}}} otherwise.
    */
  def /?(other: P): Boolean = abs(col - other.col) == abs(row - other.row)

  /**
    * Checks if another [[Position]] is on the same row, column or diagonal.
    *
    * @param other the secondary [[Position]] to consider
    * @return {{{true}}} if the two positions are reachable, {{{false}}} otherwise.
    */
  def *?(other: P): Boolean = this.-?(other) || this.|?(other) || this./?(other)


  /**
    * Calculates all the positions that are reachable with a top movement
    * of a certain number of steps.
    *
    * @param n the number of steps to consider
    * @return the list of the positions encountered during the path.
    */
  def ^^(n: Int): List[P]

  /**
    * Calculates all the positions that are reachable with a right movement
    * of a certain number of steps.
    *
    * @param n the number of steps to consider
    * @return the list of the positions encountered during the path.
    */
  def >>(n: Int): List[P]

  /**
    * Calculates all the positions that are reachable with a bottom movement
    * of a certain number of steps.
    *
    * @param n the number of steps to consider
    * @return the list of the positions encountered during the path.
    */
  def vv(n: Int): List[P]

  /**
    * Calculates all the positions that are reachable with a left movement
    * of a certain number of steps.
    *
    * @param n the number of steps to consider
    * @return the list of the positions encountered during the path.
    */
  def <<(n: Int): List[P]

}