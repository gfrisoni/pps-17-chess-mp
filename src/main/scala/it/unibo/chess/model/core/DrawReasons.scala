package it.unibo.chess.model.core

/**
  * Defines all the possible reasons for a chess draw ending.
  */
object DrawReasons {

  /**
    * Represents a draw reason.
    */
  trait DrawReason

  /**
    * Both players agreed to end in Draw.
    */
  case object MutualDraw extends DrawReason

  /**
    * A player claimed the Draw with the 50 moves rule.
    */
  case object FiftyMovesDraw extends DrawReason

  /**
    * A player ended up in Stalemate.
    */
  case object StalemateDraw extends DrawReason

}
