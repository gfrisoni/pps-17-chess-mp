package it.unibo.chess.view.general.scenes

import it.unibo.chess.view.fx.utilities.SettingPreferences
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.SettingsSceneObserver

/**
  * Represents a scene when player can choose his own preferences about the game
  */
trait SettingsScene extends ObservableView[SettingsSceneObserver] with GenericScene {

  /**
    * Read current preferences from a soruce and show them in the scene
    * @param settingPreferences current preferences
    */
  def showCurrentPreferences(settingPreferences: SettingPreferences)

}