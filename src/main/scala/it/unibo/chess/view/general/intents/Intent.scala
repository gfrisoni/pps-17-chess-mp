package it.unibo.chess.view.general.intents

import it.unibo.chess.view.general.SceneType

/**
  * Represents an intent to change current scene in a new one
  * @param sceneType type of the scene that will replace the current one
  */
class Intent(val sceneType: SceneType.Value)
