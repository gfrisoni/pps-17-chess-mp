package it.unibo.chess.view.general.observers

/**
  * Represents all the views that can be observed
  */
trait ViewObserver
