package it.unibo.chess.view.general.scenes.gameCreator

import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.view.general.ObservableView
import it.unibo.chess.view.general.observers.ViewObserver
import it.unibo.chess.view.general.scenes.GenericScene

import scala.concurrent.duration.FiniteDuration

/**
  * Represents a basic game creator. A game will surely have a game mode, a time and should be able to display
  * all available game modes
  * @tparam T observer that can observe this type of view
  */
trait GameCreatorScene[T <: ViewObserver] extends ObservableView[T] with GenericScene {

  /**
    * Get the selected game mode
    * @return selected game mode
    */
  def getSelectedGameMode: ChessMode

  /**
    * Get the selected time
    * @return selected time
    */
  def getSelectedTime: FiniteDuration

  /**
    * Display all available game modes
    * @param gameModes available game modes
    */
  def setSelectableGameModes(gameModes: Seq[ChessMode])

}
