package it.unibo.chess.view.general.observers

import java.io.File

/**
  * Represents the observer for the [[it.unibo.chess.view.general.scenes.GameScene]]
  */
trait GameSceneObserver extends BoardViewObserver with DetailsViewObserver with ViewObserver {

  /**
    * Action performed when view is ready
    */
  def onViewLoadCompletion(): Unit

  /**
    * Action performed when a user wants to save a PGN file
    * @param pgn string representing the PGN to save
    * @param file file where save the PGN
    */
  def onPgnSaving(pgn: String, file: File): Unit

}