package it.unibo.chess.view.general.observers

import it.unibo.chess.controller.database.ChessGameMongo

/**
  * Represents the observer for [[it.unibo.chess.view.fx.views.GamePreview]]
  */
trait GamePreviewObserver extends ViewObserver {

  /**
    * Action performed when the game is selected
    * @param game selected game
    */
  def onGameSelected(game: ChessGameMongo): Unit

}
