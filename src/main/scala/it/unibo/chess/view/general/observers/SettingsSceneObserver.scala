package it.unibo.chess.view.general.observers

import it.unibo.chess.view.fx.utilities.SettingPreferences

/**
  * Represents the observer for the [[it.unibo.chess.view.general.scenes.SettingsScene]]
  */
trait SettingsSceneObserver extends ViewObserver {

  /**
    * Action performed when user what to go back on previous menu
    */
  def onBack(): Unit

  /**
    * Action performed when user want to save his preferences
    * @param settingPreferences new preferences to save
    */
  def onApply(settingPreferences: SettingPreferences): Unit

}