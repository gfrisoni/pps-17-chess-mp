package it.unibo.chess.view.fx.scenes

import it.unibo.chess.view.GenericWindow
import it.unibo.chess.view.fx.GenericFXView
import it.unibo.chess.view.fx.utilities.{BoardStyles, SettingPreferences}
import it.unibo.chess.view.general.scenes.SettingsScene
import javafx.application.Platform
import javafx.collections.{FXCollections, ObservableList}
import javafx.fxml.FXML
import javafx.scene.control._

import scala.collection.JavaConverters

/**
  * Represents the fx view implementation for the [[SettingsScene]]. It permit to setup some parameters for the game.
  * @param windowManager window manager for this scene
  */
case class FXSettingsScene(override val windowManager: GenericWindow) extends GenericFXView(Some("SettingsScene.fxml")) with SettingsScene {

  @FXML protected var legalMovesEnableTgb, legalMovesDisableTgb, lastMoveEnableTgb, lastMoveDisableTgb: ToggleButton = _
  @FXML protected var stylesCmb: ComboBox[BoardStyles.Value] = _
  @FXML protected var backBtn, applyBtn: Button = _

  Platform.runLater(() => {
    // Add all available styles
    val stylesList: ObservableList[BoardStyles.Value] = FXCollections.observableArrayList[BoardStyles.Value]
    stylesList.addAll(JavaConverters.seqAsJavaList(BoardStyles.boardStyles))
    stylesCmb.setItems(stylesList)

    // Creates two groups for toggle button grouping
    val legalMovesGroup, lastMovesGroup = new ToggleGroup()
    legalMovesEnableTgb.setToggleGroup(legalMovesGroup)
    legalMovesDisableTgb.setToggleGroup(legalMovesGroup)
    lastMoveEnableTgb.setToggleGroup(lastMovesGroup)
    lastMoveDisableTgb.setToggleGroup(lastMovesGroup)
  })

  backBtn.setOnMouseClicked(_ => {
    observers.foreach(observer => observer.onBack())
  })

  applyBtn.setOnMouseClicked(_ => {
    if (stylesCmb.getSelectionModel.isEmpty)
      stylesCmb.getSelectionModel.selectFirst()

    observers.foreach(_.onApply(SettingPreferences(legalMovesEnableTgb.isSelected, lastMoveEnableTgb.isSelected, stylesCmb.getSelectionModel.getSelectedItem)))
  })

  override def showCurrentPreferences(settingPreferences: SettingPreferences): Unit = {
    legalMovesEnableTgb.setSelected(settingPreferences.isLegalMovesHighlightingEnabled)
    legalMovesDisableTgb.setSelected(!settingPreferences.isLegalMovesHighlightingEnabled)
    lastMoveEnableTgb.setSelected(settingPreferences.isLastMoveHighlightingEnabled)
    lastMoveDisableTgb.setSelected(!settingPreferences.isLastMoveHighlightingEnabled)
    stylesCmb.getSelectionModel.select(settingPreferences.boardStyle)
  }

}
