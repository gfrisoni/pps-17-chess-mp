package it.unibo.chess.view.fx.utilities

import scala.language.implicitConversions

/**
  * Represents all the styles a board can assume
  */
object BoardStyles extends Enumeration {

  protected case class Val(cssFileName: String, chessImageFolder: String) extends super.Val {
    override def toString(): String = chessImageFolder.capitalize.replace("-", " ")
  }

  implicit def valueToBoardStyleTypeVal(x: Value): Val = x.asInstanceOf[Val]

  val Default = Val("DefaultStyle.css", "default")
  val Realistic = Val("RealisticStyle.css", "realistic")
  val Metro = Val("MetroStyle.css", "metro")

  val boardStyles = Seq(Default, Realistic, Metro)

}