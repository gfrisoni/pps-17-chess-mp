package it.unibo.chess.view

import it.unibo.chess.view.MessageTypes.MessageType
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent

trait GenericWindow {

  /**
    * Show the window.
    */
  def showView(): Unit

  /**
    * Close the window.
    */
  def closeView(): Unit

  /**
    * Check if the window is currently showing
    * @return true if window is visible, false if it's closed.
    */
  def isShowing: Boolean

  /**
    * Set the title of the window
    * @param title title of the window
    */
  def setTitle(title: String): Unit

  /**
    * Get the title of the window
    * @return the title of the window.
    */
  def getTitle: String

  /**
    * Set the scene inside of the window, getting its type from the intent.
    * @param intent Intent to change current scene into another
    */
  def setScene(intent: Intent)

  /**
    * Get the current visible scene type.
    * @return type of the visible scene. None if there is no scene visible.
    */
  def getScene: Option[SceneType.Value]

  /**
    * Shows a message to user.
    * @param message string representing the message that will be shown
    * @param messageType type of the message
    */
  def showMessage(message: String, messageType: MessageType): Unit

  /**
    * Show a message to user, with title
    * @param title string representing the title of the message
    * @param message string representing the message
    * @param messageType type of the message
    */
  def showMessage(title: String, message: String, messageType: MessageType): Unit

}
