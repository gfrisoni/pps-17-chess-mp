package it.unibo.chess.model.pgn

import it.unibo.chess.model.pgn.PgnTagTypes.Result

import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

import scalaz.NonEmptyList

@RunWith(classOf[JUnitRunner])
class PgnMovesParserSpecs extends FunSpec with Matchers {

  describe("A PGN moves parser") {

    describe("should be able to convert successfully") {

      describe("a correct string with only one PGN move in its object representation") {

        it("related to a pawn movement") {
          val pawnMove: String = "1. a3"
          PgnMovesParser(pawnMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("a3", Seq.empty)), None)))
        }

        it("related to a not pawn movement") {
          val knightMove: String = "1. Na3"
          val bishopMove: String = "1. Bf4"
          val rookMove: String = "1. Rh8"
          val queenMove: String = "1. Qd5"
          val kingMove: String = "1. Kf1"
          PgnMovesParser(knightMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Na3", Seq.empty)), None)))
          PgnMovesParser(bishopMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Bf4", Seq.empty)), None)))
          PgnMovesParser(rookMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Rh8", Seq.empty)), None)))
          PgnMovesParser(queenMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Qd5", Seq.empty)), None)))
          PgnMovesParser(kingMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Kf1", Seq.empty)), None)))
        }

        it("related to a pawn capture") {
          val pawnCapture: String = "1. exd4"
          PgnMovesParser(pawnCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("exd4", Seq.empty)), None)))
        }

        it("related to a not pawn capture") {
          val knightCapture: String = "1. Nxa3"
          val bishopCapture: String = "1. Bxf4"
          val rookCapture: String = "1. Rxh8"
          val queenCapture: String = "1. Qxd5"
          val kingCapture: String = "1. Kxf1"
          PgnMovesParser(knightCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Nxa3", Seq.empty)), None)))
          PgnMovesParser(bishopCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Bxf4", Seq.empty)), None)))
          PgnMovesParser(rookCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Rxh8", Seq.empty)), None)))
          PgnMovesParser(queenCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Qxd5", Seq.empty)), None)))
          PgnMovesParser(kingCapture) should be(scalaz.Success((Seq.empty, Seq(PgnMove("Kxf1", Seq.empty)), None)))
        }

        it("related to a pawn capture and promotion") {
          val pawnCapturePromote: String = "1. bxc8=Q"
          PgnMovesParser(pawnCapturePromote) should be(scalaz.Success((Seq.empty, Seq(PgnMove("bxc8=Q", Seq.empty)), None)))
        }

        it("related to a pawn promotion") {
          val pawnPromote: String = "1. c8=Q"
          PgnMovesParser(pawnPromote) should be(scalaz.Success((Seq.empty, Seq(PgnMove("c8=Q", Seq.empty)), None)))
        }

        it("related to a castling") {
          val shortCastling: String = "1. 0-0"
          val longCastling: String = "1. 0-0-0"
          PgnMovesParser(shortCastling) should be(scalaz.Success((Seq.empty, Seq(PgnMove("0-0", Seq.empty)), None)))
          PgnMovesParser(longCastling) should be(scalaz.Success((Seq.empty, Seq(PgnMove("0-0-0", Seq.empty)), None)))
        }

        it("related to a game result") {
          val unfinishedResult: String = "*"
          val drawResult: String = "½-½"
          val whiteWinnerResult: String = "1-0"
          val whiteLoserResult: String = "0-1"
          PgnMovesParser(unfinishedResult) should be(scalaz.Success((Seq.empty, Seq.empty, Some(PgnTag(Result, "*").toOption.get))))
          PgnMovesParser(drawResult) should be(scalaz.Success((Seq.empty, Seq.empty, Some(PgnTag(Result, "½-½").toOption.get))))
          PgnMovesParser(whiteWinnerResult) should be(scalaz.Success((Seq.empty, Seq.empty, Some(PgnTag(Result, "1-0").toOption.get))))
          PgnMovesParser(whiteLoserResult) should be(scalaz.Success((Seq.empty, Seq.empty, Some(PgnTag(Result, "0-1").toOption.get))))
        }

        it("related to a move with an initial comment") {
          val initialCommentMove: String = "{Interesting game} 1. a4"
          val initialCommentsMove: String = "{Interesting game}{Beautiful game} 1. a4"
          PgnMovesParser(initialCommentMove) should be(scalaz.Success((Seq("Interesting game"), Seq(PgnMove("a4", Seq.empty)), None)))
          PgnMovesParser(initialCommentsMove) should be(scalaz.Success((
            Seq("Interesting game", "Beautiful game"),
            Seq(PgnMove("a4", Seq.empty)),
            None
          )))
        }

        it("related to a move with a related block comment") {
          val blockCommentMove: String = "1. a4 {First white move}"
          val inlineCommentMove: String = "1. a4 ;First white move"
          PgnMovesParser(blockCommentMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("a4", Seq("First white move"))), None)))
          PgnMovesParser(inlineCommentMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("a4", Seq("First white move"))), None)))
        }

        it("related to a move with check flag") {
          val checkMove: String = "1. d7+"
          PgnMovesParser(checkMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("d7+", Seq.empty)), None)))
        }

        it("related to a move with checkmate flag") {
          val checkmateMove: String = "1. d7#"
          PgnMovesParser(checkmateMove) should be(scalaz.Success((Seq.empty, Seq(PgnMove("d7#", Seq.empty)), None)))
        }

      }

      describe("a correct string with more PGN moves in their objects representation") {

        val pgnWithoutComments: String = "1. d4 d5 2.c4 dxc4 3.e4 e5 4.Nf3 Bb4+"
        val pgnWithComments: String = "{Game used as model} 1. e4 Nf6 {Alekhine's defense} 2.e5 {Second White's move}" +
          "{Pawn in e5} Nd5 3.d4 d6 4.Nf3 g6"

        describe("without result") {

          it("without moves comments") {
            PgnMovesParser(pgnWithoutComments) should be(scalaz.Success((
              Seq.empty,
              Seq(
                PgnMove("d4", Seq.empty),
                PgnMove("d5", Seq.empty),
                PgnMove("c4", Seq.empty),
                PgnMove("dxc4", Seq.empty),
                PgnMove("e4", Seq.empty),
                PgnMove("e5", Seq.empty),
                PgnMove("Nf3", Seq.empty),
                PgnMove("Bb4+", Seq.empty)
              ),
              None
            )))
          }

          it("with moves comments") {
            PgnMovesParser(pgnWithComments) should be(scalaz.Success((
              Seq("Game used as model"),
              Seq(
                PgnMove("e4", Seq.empty),
                PgnMove("Nf6", Seq("Alekhine's defense")),
                PgnMove("e5", Seq("Second White's move", "Pawn in e5")),
                PgnMove("Nd5", Seq.empty),
                PgnMove("d4", Seq.empty),
                PgnMove("d6", Seq.empty),
                PgnMove("Nf3", Seq.empty),
                PgnMove("g6", Seq.empty)
              ),
              None
            )))
          }

        }

        describe("with result") {

          it("without moves comments") {
            PgnMovesParser(pgnWithoutComments.concat(" *")) should be(scalaz.Success((
              Seq.empty,
              Seq(
                PgnMove("d4", Seq.empty),
                PgnMove("d5", Seq.empty),
                PgnMove("c4", Seq.empty),
                PgnMove("dxc4", Seq.empty),
                PgnMove("e4", Seq.empty),
                PgnMove("e5", Seq.empty),
                PgnMove("Nf3", Seq.empty),
                PgnMove("Bb4+", Seq.empty)
              ),
              Some(PgnTag(PgnTagTypes.Result, "*").toOption.get)
            )))
          }

          it("with moves comments") {
            PgnMovesParser(pgnWithComments.concat(" 1-0")) should be(scalaz.Success((
              Seq("Game used as model"),
              Seq(
                PgnMove("e4", Seq.empty),
                PgnMove("Nf6", Seq("Alekhine's defense")),
                PgnMove("e5", Seq("Second White's move", "Pawn in e5")),
                PgnMove("Nd5", Seq.empty),
                PgnMove("d4", Seq.empty),
                PgnMove("d6", Seq.empty),
                PgnMove("Nf3", Seq.empty),
                PgnMove("g6", Seq.empty)
              ),
              Some(PgnTag(Result, "1-0").toOption.get)
            )))
          }

        }

      }

    }

    describe("should fail if the string with PGN moves contains errors") {

      it("related to invalid moves") {
        val moveWithInvalidChars: String = "1. e4!"
        val moveWithInvalidPattern1: String = "1. e40-0B"
        val moveWithInvalidPattern2: String = "1. #e4"
        PgnMovesParser(moveWithInvalidChars) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: ';' expected but '!' found""".stripMargin)))
        PgnMovesParser(moveWithInvalidPattern1) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: ';' expected but 'B' found""".stripMargin)))
        PgnMovesParser(moveWithInvalidPattern2) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: string matching regex '0\-0\-0|0\-0|(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)' expected but '#' found""".stripMargin)))
      }

      it("related to invalid enumeration") {
        val moveWithInvalidEnumeration: String = "1a. e4"
        PgnMovesParser(moveWithInvalidEnumeration) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: string matching regex '0\-0\-0|0\-0|(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)' expected but 'a' found""".stripMargin)))
      }

      it("related to invalid commentary") {
        val leftBracketNotOpened: String = "1. e4 Test}"
        val rightBracketNotClosed: String = "1. e4 {Test"
        PgnMovesParser(leftBracketNotOpened) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: ';' expected but 'T' found""".stripMargin)))
        PgnMovesParser(rightBracketNotClosed) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: '}' expected but end of source found""".stripMargin)))
      }

      it("related to invalid results") {
        val notExistingResult1: String = "2-0"
        val notExistingResult2: String = "**"
        PgnMovesParser(notExistingResult1) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: string matching regex '0\-0\-0|0\-0|(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)' expected but '-' found""".stripMargin)))
        PgnMovesParser(notExistingResult2) should be(scalaz.Failure(NonEmptyList(
          """Cannot parse PGN moves.
            |An error occurred: ';' expected but '*' found""".stripMargin)))
      }

    }

  }

}