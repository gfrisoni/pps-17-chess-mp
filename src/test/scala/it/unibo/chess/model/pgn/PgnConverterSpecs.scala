package it.unibo.chess.model.pgn

import com.github.nscala_time.time.Imports._
import it.unibo.chess.GameExamples.foolMate
import it.unibo.chess.model.core.ChessRoles.{Bishop, Knight, Pawn}
import it.unibo.chess.model.core.DrawReasons.MutualDraw
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.history.ChessActions.{CaptureAction, MoveAction}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

@RunWith(classOf[JUnitRunner])
class PgnConverterSpecs extends FunSpec with Matchers {

  val game: ChessGame = ChessGame(
    players = Map(
      White -> ChessPlayer("Giacomo Frisoni"),
      Black -> ChessPlayer("Leonardo Montini")),
    dateTime = (new DateTime).withYear(2018).withMonthOfYear(9).withDayOfMonth(15)
  )

  describe("A PGN converter") {

    describe("should be able to calculate the PGN string of a game result") {

      it("if present") {
        val finishedGame: ChessGame = game.move('c2, 'c3).move('b8, 'c6).draw(MutualDraw)
        PgnConverter.resultToPgn(finishedGame.history) should be(Some("½-½"))
      }

      it("returning an empty value if not present") {
        PgnConverter.resultToPgn(game.history).isEmpty should be(true)
      }

    }

    describe("should be able to calculate the PGN string of a game history") {

      it("if it has registered game steps") {
        val nonEmptyGame: ChessGame = game
          .move('c2, 'c3).move('b8, 'c6)
          .move('d2, 'd4).move('c6, 'd4)
          .move('c3, 'd4)
        PgnConverter.historyToPgn(nonEmptyGame.history) should be("1. c3 Nc6 2. d4 Nxd4 3. cxd4")
      }

      it("returning an empty value if it does not have registered game steps") {
        PgnConverter.historyToPgn(game.history) should be("")
      }

    }

    describe("should be able to calculate the PGN object representation of a game history") {

      it("related to a game without registered game steps") {
        PgnConverter.historyToInteractivePgn(game.history) should be((Map.empty, None))
      }

      describe("related to an unfinished and non empty game") {

        it("with complete full moves") {
          val completeMovesGame: ChessGame = game
            .move('c2, 'c3).move('b8, 'c6)
            .move('d2, 'd4).move('c6, 'd4)
          PgnConverter.historyToInteractivePgn(completeMovesGame.history) should be(
            (Map(
              1 -> PgnFullMove("c3", Some("Nc6")),
              2 -> PgnFullMove("d4", Some("Nxd4"))
            ), None)
          )
        }

        it("with an incomplete full move") {
          val incompleteMovesGame: ChessGame = game
            .move('c2, 'c3).move('b8, 'c6)
            .move('d2, 'd4)
          PgnConverter.historyToInteractivePgn(incompleteMovesGame.history) should be(
            (Map(
              1 -> PgnFullMove("c3", Some("Nc6")),
              2 -> PgnFullMove("d4", None)
            ), None)
          )
        }

      }

      describe("related to a finished game") {

        it("with complete full moves") {
          PgnConverter.historyToInteractivePgn(foolMate.history) should be(
            (Map(
              1 -> PgnFullMove("f3", Some("e5")),
              2 -> PgnFullMove("g4", Some("Qh4#"))
            ), Some("0-1"))
          )
        }

        it("with an incomplete full move") {
          val incompleteMovesGame: ChessGame = game
            .move('c2, 'c3).move('b8, 'c6)
            .move('d2, 'd4).draw(MutualDraw)
          PgnConverter.historyToInteractivePgn(incompleteMovesGame.history) should be(
            (Map(
              1 -> PgnFullMove("c3", Some("Nc6")),
              2 -> PgnFullMove("d4", None)
            ), Some("½-½"))
          )
        }

      }

    }

    describe("should be able to calculate the PGN string of a game") {

      it("without moves") {
        PgnConverter.gameToPgn(game) should be(
          """[Event "Casual Game"]
            |[Site "Chess Multiplayer"]
            |[Date "2018.09.15"]
            |[Round "-"]
            |[White "Giacomo Frisoni"]
            |[Black "Leonardo Montini"]
            |[Result "*"]""".stripMargin
        )
      }

      describe("with moves") {

        it("not finished") {
          val notFinishedGame: ChessGame = game.move('a2, 'a3).move('b7, 'b5).move('b1, 'c3)
          PgnConverter.gameToPgn(notFinishedGame) should be(
            """[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "*"]
              |1. a3 b5 2. Nc3""".stripMargin
          )
        }

        it("finished in draw") {
          val drawGame: ChessGame = game.move('a2, 'a3).move('c7, 'c5).draw(MutualDraw)
          PgnConverter.gameToPgn(drawGame) should be(
            """[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "½-½"]
              |1. a3 c5 ½-½""".stripMargin
          )
        }

        describe("finished with a winning") {

          it("white player") {
            val whiteWinnerGame: ChessGame = ChessGame.copy(
              FenParser
                .fromFen("6k1/5ppp/8/8/8/8/8/R3K3 w KQkq - 0 1")
                .move('a1, 'a8)
            )(newDateTime = (new DateTime).withYear(2018).withMonthOfYear(9).withDayOfMonth(26))
            PgnConverter.gameToPgn(whiteWinnerGame) should be(
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.26"]
                |[Round "-"]
                |[White "player1"]
                |[Black "player2"]
                |[Result "1-0"]
                |1. Ra8# 1-0""".stripMargin
            )
          }

          it("black player") {
            val blackWinnerGame: ChessGame = foolMate
            PgnConverter.gameToPgn(blackWinnerGame) should be(
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.15"]
                |[Round "-"]
                |[White "player1"]
                |[Black "player2"]
                |[Result "0-1"]
                |1. f3 e5 2. g4 Qh4# 0-1""".stripMargin
            )
          }

        }

      }

    }

    describe("should be able to calculate a game from the related PGN string") {

      describe("and handle pgn tag delimiters") {

        it("presence") {
          val pgn: String =
            """[pgn]
              |[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "*"]
              |[/pgn]""".stripMargin
          PgnConverter.fromPgn(pgn) onComplete {
            case Success(parsedGame) => parsedGame should be(
              ChessGame(
                players = Map(White -> ChessPlayer("Giacomo Frisoni"), Black -> ChessPlayer("Leonardo Montini")),
                dateTime = (new DateTime).withDate(2018, 9, 15)
                  .withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
              )
            )
            case Failure(exception) => fail(exception)
          }
        }

        it("absence") {
          val pgn: String =
            """[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "*"]""".stripMargin
          PgnConverter.fromPgn(pgn) onComplete {
            case Success(parsedGame) => parsedGame should be(
              ChessGame(
                players = Map(White -> ChessPlayer("Giacomo Frisoni"), Black -> ChessPlayer("Leonardo Montini")),
                dateTime = (new DateTime).withDate(2018, 9, 15)
                  .withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
              )
            )
            case Failure(exception) => fail(exception)
          }
        }

      }

      describe("and handle tags and moves") {

        it("separated by a new line") {
          val pgn: String =
            """[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "*"]
              |1.d4 d5 2.c4 dxc4 3.e4 e5 4.Nf3 Bb4+
            """.stripMargin
          val nextGameFromFen: ChessGame = FenParser.fromFen("rnbqk1nr/ppp2ppp/8/4p3/1bpPP3/5N2/PP3PPP/RNBQKB1R w KQkq - 0 1")
          PgnConverter.fromPgn(pgn) onComplete {
            case Success(parsedGame) =>
              parsedGame.board should be(nextGameFromFen.board.copy(taken = List(
                ChessPiece(White, Pawn(White))
              )))
              parsedGame.players should be(Map(White -> ChessPlayer("Giacomo Frisoni"), Black -> ChessPlayer("Leonardo Montini")))
              parsedGame.dateTime.getYear should be(2018)
              parsedGame.dateTime.getMonthOfYear should be(9)
              parsedGame.dateTime.getDayOfMonth should be(15)
              parsedGame.history.gameSteps.map(_.action).seq should be(Seq(
                MoveAction(
                  (ChessPiece(White, Pawn(White)), 'd2),
                  AmbiguityTypes.None,
                  'd4
                ),
                MoveAction(
                  (ChessPiece(Black, Pawn(Black)), 'd7),
                  AmbiguityTypes.None,
                  'd5
                ),
                MoveAction(
                  (ChessPiece(White, Pawn(White)), 'c2),
                  AmbiguityTypes.None,
                  'c4
                ),
                CaptureAction(
                  (ChessPiece(Black, Pawn(Black)), 'd5),
                  AmbiguityTypes.None,
                  'c4,
                  ChessPiece(White, Pawn(White))
                ),
                MoveAction(
                  (ChessPiece(White, Pawn(White)), 'e2),
                  AmbiguityTypes.None,
                  'e4
                ),
                MoveAction(
                  (ChessPiece(Black, Pawn(Black)), 'e7),
                  AmbiguityTypes.None,
                  'e5
                ),
                MoveAction(
                  (ChessPiece(White, Knight), 'g1),
                  AmbiguityTypes.None,
                  'f3
                ),
                MoveAction(
                  (ChessPiece(Black, Bishop), 'f8),
                  AmbiguityTypes.None,
                  'b4,
                  KingStates.Check
                )))
            case Failure(exception) => fail(exception)
          }
        }

        it("not separated by a new line") {
          val pgn: String =
            """[Event "Casual Game"]
              |[Site "Chess Multiplayer"]
              |[Date "2018.09.15"]
              |[Round "-"]
              |[White "Giacomo Frisoni"]
              |[Black "Leonardo Montini"]
              |[Result "*"]1.Nc3 d5 2.Nxd5 c6 3.Nc7+
            """.stripMargin
          val nextGameFromFen: ChessGame = FenParser.fromFen("rnbqkbnr/ppN1pppp/2p5/8/8/8/PPPPPPPP/R1BQKBNR w KQkq - 0 1")
          PgnConverter.fromPgn(pgn) onComplete {
            case Success(parsedGame) =>
              parsedGame.board should be(nextGameFromFen.board.copy(taken = List(
                ChessPiece(Black, Pawn(Black))
              )))
              parsedGame.players should be(Map(White -> ChessPlayer("Giacomo Frisoni"), Black -> ChessPlayer("Leonardo Montini")))
              parsedGame.dateTime.getYear should be(2018)
              parsedGame.dateTime.getMonthOfYear should be(9)
              parsedGame.dateTime.getDayOfMonth should be(15)
              parsedGame.history.gameSteps.map(_.action).seq should be(Seq(
                MoveAction(
                  (ChessPiece(White, Knight), 'b1),
                  AmbiguityTypes.None,
                  'c3
                ),
                MoveAction(
                  (ChessPiece(Black, Pawn(Black)), 'd7),
                  AmbiguityTypes.None,
                  'd5
                ),
                CaptureAction(
                  (ChessPiece(White, Knight), 'c3),
                  AmbiguityTypes.None,
                  'd5,
                  ChessPiece(Black, Pawn(Black))
                ),
                MoveAction(
                  (ChessPiece(Black, Pawn(Black)), 'c7),
                  AmbiguityTypes.None,
                  'c6
                ),
                MoveAction(
                  (ChessPiece(White, Knight), 'd5),
                  AmbiguityTypes.None,
                  'c7,
                  KingStates.Check
                )))
            case Failure(exception) => fail(exception)
          }
        }

      }

      it("and handle Fen PGN tag") {
        val pgnWithFenTag: String =
          """[Event "Casual Game"]
            |[Site "Chess Multiplayer"]
            |[Date "2018.09.15"]
            |[Round "-"]
            |[White "Giacomo Frisoni"]
            |[Black "Leonardo Montini"]
            |[Result "*"]
            |[Fen "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"]
            |1.a3 b6""".stripMargin
        val nextGameFromFen: ChessGame = FenParser.fromFen("rnbqkbnr/p1pppppp/1p6/8/8/P7/1PPPPPPP/RNBQKBNR w KQkq - 0 1")
        PgnConverter.fromPgn(pgnWithFenTag) onComplete {
          case Success(parsedGame) =>
            parsedGame.board should be(nextGameFromFen.board)
            parsedGame.players should be(Map(White -> ChessPlayer("Giacomo Frisoni"), Black -> ChessPlayer("Leonardo Montini")))
            parsedGame.dateTime.getYear should be(2018)
            parsedGame.dateTime.getMonthOfYear should be(9)
            parsedGame.dateTime.getDayOfMonth should be(15)
            parsedGame.history.gameSteps.map(_.action).seq should be(Seq(
              MoveAction(
                (ChessPiece(White, Pawn(White)), 'a2),
                AmbiguityTypes.None,
                'a3
              ),
              MoveAction(
                (ChessPiece(Black, Pawn(Black)), 'b7),
                AmbiguityTypes.None,
                'b6
              )))
          case Failure(exception) => fail(exception)
        }
      }
    }

    it("and notify an observer properly") {
      val pgn: String =
        """[Event "Casual Game"]
          |[Site "Chess Multiplayer"]
          |[Date "2018.09.15"]
          |[Round "-"]
          |[White "Giacomo Frisoni"]
          |[Black "Leonardo Montini"]
          |[Result "*"]
          |1.a3 b6""".stripMargin
      var nMovesOpt: Option[Int] = None
      var parsedMoves: Seq[String] = Seq.empty
      PgnConverter.fromPgn(pgn, Some(new FromPgnObserver {
        override def totalMoves(nMoves: Int): Unit = nMovesOpt = Some(nMoves)
        override def moveParsed(san: String): Unit = parsedMoves = parsedMoves :+ san
      })) onComplete {
        case Success(_) =>
          nMovesOpt should be(Some(2))
          parsedMoves should be(Seq("a3", "b6"))
        case Failure(exception) => fail(exception)
      }
    }

  }

  describe("should not be able to calculate the PGN string of a game") {

    describe("with invalid PGN tags") {

      it("related to a tag with a syntactic error") {
        val pgnWithInvalidTags: String =
          """[Event Casual Game"]
            |[Site "Chess Multiplayer"]
            |[Date "2018.09.15"]
            |[Round "-"]
            |[White "Giacomo Frisoni"]
            |[Black "Leonardo Montini"]
            |[Result "*"]1.Nc3 d5 2.Nxd5 c6 3.Nc7+
          """.stripMargin
        PgnConverter.fromPgn(pgnWithInvalidTags) onComplete {
          case Success(_) => fail()
          case Failure(exception) => exception.getMessage should be(
            raw"""Invalid PGN tags: Cannot parse PGN tags.
                 |An error occurred: '"' expected but 'C' found""".stripMargin)
        }
      }

      it("related to a tag with an incorrect format") {
        val pgnWithInvalidTags: String =
          """[Event "Casual Game"]
            |[Site "Chess Multiplayer"]
            |[Date "2018/09/15"]
            |[Round "-"]
            |[White "Giacomo Frisoni"]
            |[Black "Leonardo Montini"]
            |[Result "*"]1.Nc3 d5 2.Nxd5 c6 3.Nc7+
          """.stripMargin
        PgnConverter.fromPgn(pgnWithInvalidTags) onComplete {
          case Success(_) => fail()
          case Failure(exception) => exception.getMessage should be(
            raw"""Invalid PGN tags: Cannot parse PGN tags. Format errors detected:
                 |The tag value '2018/09/15' does not satisfy the format required by 'Date' type""".stripMargin)
        }

      }

      describe("with invalid PGN moves") {

        describe("related to an incorrect SAN move") {

          it("caused by a syntax error") {
            val pgnWithInvalidSanMove: String =
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.15"]
                |[Round "-"]
                |[White "Giacomo Frisoni"]
                |[Black "Leonardo Montini"]
                |[Result "*"]1.NNc3 d5
              """.stripMargin
            PgnConverter.fromPgn(pgnWithInvalidSanMove) onComplete {
              case Success(_) => fail()
              case Failure(exception) => exception.getMessage should be(
                raw"""Invalid PGN moves: Cannot parse PGN moves.
                     |An error occurred: string matching regex '0\-0\-0|0\-0|(N|B|R|Q|K|)([a-h]?)([1-8]?)(x?)([a-h][0-9])(=?[NBRQ]?)(\+?)(\#?)' expected but 'N' found""".stripMargin)
            }
          }

          it("caused by an illegal move") {
            val pgnWithIllegalMove: String =
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.15"]
                |[Round "-"]
                |[White "Giacomo Frisoni"]
                |[Black "Leonardo Montini"]
                |[Result "*"]1.b5 d5
              """.stripMargin
            PgnConverter.fromPgn(pgnWithIllegalMove) onComplete {
              case Success(_) => fail()
              case Failure(exception) => exception.getMessage should be(
                "An error occurred during a move: The move b5 is illegal for the chess role Pawn(White)"
              )
            }
          }

          it("related to an incorrect FEN tag") {
            val pgnWithInvalidFenTag: String =
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.15"]
                |[Round "-"]
                |[White "Giacomo Frisoni"]
                |[Black "Leonardo Montini"]
                |[Result "*"]
                |[Fen "rnbqkppp/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"]
                |1.a3 b6""".stripMargin
            PgnConverter.fromPgn(pgnWithInvalidFenTag) onComplete {
              case Success(_) => fail()
              case Failure(exception) => exception.getMessage should be(
                "An error occurred during the game creation: FEN=rnbqkppp"
              )
            }

          }

          it("with incomplete Seven Tag Roster") {
            val incompletePgn: String =
              """[Event "Casual Game"]
                |[Site "Chess Multiplayer"]
                |[Date "2018.09.15"]
                |[Result "*"]1.a5 d5
              """.stripMargin
            PgnConverter.fromPgn(incompletePgn) onComplete {
              case Success(_) => fail()
              case Failure(exception) => exception.getMessage should be(
                "The specified PGN tags are not sufficient according to the Seven Tag Roster"
              )
            }
          }

        }

      }
    }

  }
}