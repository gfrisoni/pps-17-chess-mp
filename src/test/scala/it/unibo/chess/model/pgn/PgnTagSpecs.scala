package it.unibo.chess.model.pgn

import it.unibo.chess.model.pgn.PgnTagTypes._
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

import scalaz.{Success, Failure, NonEmptyList}

@RunWith(classOf[JUnitRunner])
class PgnTagSpecs extends FunSpec with Matchers {

  describe("A PGN tag") {

    describe("with a known type") {

      it("and a valid format should be recognized") {
        PgnTag("event", "Casual Game") should be(Success(PgnTag(Event, "Casual Game").toOption.get))
        PgnTag("site", "Chess Multiplayer") should be(Success(PgnTag(Site, "Chess Multiplayer").toOption.get))
        PgnTag("date", "2018.09.10") should be(Success(PgnTag(Date, "2018.09.10").toOption.get))
        PgnTag("time", "11:38:00") should be(Success(PgnTag(Time, "11:38:00").toOption.get))
        PgnTag("round", "-") should be(Success(PgnTag(Round, "-").toOption.get))
        PgnTag("round", "1") should be(Success(PgnTag(Round, "1").toOption.get))
        PgnTag("round", "3.1") should be(Success(PgnTag(Round, "3.1").toOption.get))
        PgnTag("round", "4.1.2") should be(Success(PgnTag(Round, "4.1.2").toOption.get))
        PgnTag("white", "Giacomo Frisoni") should be(Success(PgnTag(White, "Giacomo Frisoni").toOption.get))
        PgnTag("black", "Leonardo Montini") should be(Success(PgnTag(Black, "Leonardo Montini").toOption.get))
        PgnTag("whiteType", "human") should be(Success(PgnTag(WhiteType, "human").toOption.get))
        PgnTag("whiteType", "program") should be(Success(PgnTag(WhiteType, "program").toOption.get))
        PgnTag("blackType", "human") should be(Success(PgnTag(BlackType, "human").toOption.get))
        PgnTag("blackType", "program") should be(Success(PgnTag(BlackType, "program").toOption.get))
        PgnTag("result", "*") should be(Success(PgnTag(Result, "*").toOption.get))
        PgnTag("result", "1-0") should be(Success(PgnTag(Result, "1-0").toOption.get))
        PgnTag("result", "0-1") should be(Success(PgnTag(Result, "0-1").toOption.get))
        PgnTag("result", "1/2-1/2") should be(Success(PgnTag(Result, "1/2-1/2").toOption.get))
        PgnTag("result", "½-½") should be(Success(PgnTag(Result, "½-½").toOption.get))
        PgnTag("termination", "Giacomo Frisoni won by resignation") should be(
          Success(PgnTag(Termination, "Giacomo Frisoni won by resignation").toOption.get)
        )
        PgnTag("fen", "3k4/8/8/8/8/8/4N1N1/3K4 w KQkq - 0 1") should be(
          Success(PgnTag(Fen, "3k4/8/8/8/8/8/4N1N1/3K4 w KQkq - 0 1").toOption.get)
        )
      }

      describe("and an invalid format") {

        it("related to Date should not be recognized") {
          PgnTag("date", "2018/09/10") should be(
            Failure(NonEmptyList("The tag value '2018/09/10' does not satisfy the format required by 'Date' type"))
          )
          PgnTag("date", "2018.14.10") should be(
            Failure(NonEmptyList("The tag value '2018.14.10' does not satisfy the format required by 'Date' type"))
          )
        }

        it("related to Time should not be recognized") {
          PgnTag("time", "11.38.00") should be(
            Failure(NonEmptyList("The tag value '11.38.00' does not satisfy the format required by 'Time' type"))
          )
        }

        it("related to WhiteType should not be recognized") {
          PgnTag("whiteType", "-") should be(
            Failure(NonEmptyList("The tag value '-' does not satisfy the format required by 'WhiteType' type"))
          )
        }

        it("related to BlackType should not be recognized") {
          PgnTag("blackType", "agent") should be(
            Failure(NonEmptyList("The tag value 'agent' does not satisfy the format required by 'BlackType' type"))
          )
        }

        it("related to Round should not be recognized") {
          PgnTag("round", "1.") should be(
            Failure(NonEmptyList("The tag value '1.' does not satisfy the format required by 'Round' type"))
          )
          PgnTag("round", "uno") should be(
            Failure(NonEmptyList("The tag value 'uno' does not satisfy the format required by 'Round' type"))
          )
        }

        it("related to Result should not be recognized") {
          PgnTag("result", "1-1") should be(
            Failure(NonEmptyList("The tag value '1-1' does not satisfy the format required by 'Result' type"))
          )
        }

        it("related to Fen should not be recognized") {
          PgnTag("fen", "rnbqkbnr/pppppppp/8/8/8/8/P") should be(
            Failure(NonEmptyList("The tag value 'rnbqkbnr/pppppppp/8/8/8/8/P' does not satisfy the format required by 'Fen' type"))
          )
        }

      }

    }

    describe("with an unknown type") {

      it("should have a correct string representation") {
        PgnTag(Unknown("Test"), "Example value").toOption.get.toString should be("""[Test "Example value"]""")
      }

      it("should be recognized") {
        PgnTag("Test", "Example value") should be(Success(PgnTag(Unknown("Test"), "Example value").toOption.get))
      }

    }

    it("should have a valid string representation") {
      PgnTag("Event", "Casual Game").toOption.get.toString should be("""[Event "Casual Game"]""")
    }

  }

}