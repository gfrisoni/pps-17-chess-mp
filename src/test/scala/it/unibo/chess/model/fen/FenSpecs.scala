package it.unibo.chess.model.fen

import it.unibo.chess.exceptions.Fen.BadFEN
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core._
import it.unibo.chess.GameExamples.advancedGameState
import it.unibo.chess.model.core.position.ChessBoardPosition
import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FenSpecs extends FunSpec {

  val game = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))

  describe("A FEN parser") {

    it("should be able to configure correctly a game from initial FEN") {
      val gameFromInitialFEN = FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
      assert(gameFromInitialFEN.board == game.board)
      assert(gameFromInitialFEN.board.taken == game.board.taken)
      assert(gameFromInitialFEN.board.pieces == game.board.pieces)
      assert(gameFromInitialFEN.currentTurn == White)
      assert(gameFromInitialFEN.flags.castlingAvailability == Map((White, ChessBoardPosition('c1)) -> true, (White, ChessBoardPosition('g1)) -> true,
        (Black, ChessBoardPosition('c8)) -> true, (Black, ChessBoardPosition('g8)) -> true))
      assert(gameFromInitialFEN.flags.enPassantTarget.isEmpty)
      assert(gameFromInitialFEN.flags.halfMove == 0)
      assert(gameFromInitialFEN.flags.fullMove == 1)
    }

    it("should be able to configure a game with parameters of a specific FEN string") {
      val newGame = game.move('e2, 'e4).move('c7, 'c5)
      val gameFromFEN = FenParser.fromFen("rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2")
      assert(gameFromFEN.board.taken == List.empty)
      assert((gameFromFEN.board.pieces.toSet diff newGame.board.pieces.toSet).toMap.isEmpty)
      assert(gameFromFEN.currentTurn == White)
      assert(gameFromFEN.flags.castlingAvailability == Map((White, ChessBoardPosition('c1)) -> true, (White, ChessBoardPosition('g1)) -> true,
        (Black, ChessBoardPosition('c8)) -> true, (Black, ChessBoardPosition('g8)) -> true))
      assert(gameFromFEN.flags.enPassantTarget.contains(ChessBoardPosition('c6)))
      assert(gameFromFEN.flags.halfMove == 0)
      assert(gameFromFEN.flags.fullMove == 2)
    }

    it("should accept only a valid FEN") {
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR KQkq - 0 1"))
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - 0 1"))
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq 0 1"))
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 1"))
    }

    it("should fail if there aren't 8 rows") {
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"))
    }

    it("should fail if there is more than 1 pawn in the respective promotion line") {
      assertThrows[BadFEN](FenParser.fromFen("rnbqkppp/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"))
      assertThrows[BadFEN](FenParser.fromFen("rnbqkppp/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBPP w KQkq - 0 1"))
      assertThrows[BadFEN](FenParser.fromFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNP w KQkq - 0 1"))
    }

    it("should fail if there are unparsable character") {
      assertThrows[BadFEN](FenParser.fromFen("rnbXXX/ppmmmppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"))
    }

    it("should create a valid FEN string from a specific game state") {
      val newGame = game.move('e2, 'e4).move('c7, 'c5)
      assert(FenParser.toFen(game) == "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
      assert(FenParser.toFen(advancedGameState) == "rnbq1bnr/pppk1ppp/8/3pp3/2B1P3/5N2/PPPP1PPP/RNBQK2R w KQ - 2 4")
      assert(FenParser.toFen(newGame) == "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2")
    }
  }
}
