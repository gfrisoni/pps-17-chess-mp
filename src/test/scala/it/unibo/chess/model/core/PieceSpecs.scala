package it.unibo.chess.model.core

import it.unibo.chess.GameExamples
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.logic.{Continue, IncludeAndStop, PathControl, Stop}
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

import scala.language.postfixOps

@RunWith(classOf[JUnitRunner])
class PieceSpecs extends FlatSpec {

  val gameState: ChessGame = GameExamples.advancedGameState

  "a Piece instantiated as White Pawn" should "move as a Pawn with color White" in {
    val pawnRole: ChessRole = Pawn(White)
    val pawnPiece: ChessPiece = ChessPiece(White, Pawn(White))
    val position: BoardPosition = 'c2
    assert((pawnRole directionsFrom position).toSet == (pawnPiece validatedDirectionsFrom position).keys)
  }

  "a Piece instantiated as White King" should "move as a King with color White" in {
    val kingRole: ChessRole = King(White)
    val kingPiece: ChessPiece = ChessPiece(White, King(White))
    val position: BoardPosition = 'e1
    assert((kingRole directionsFrom position).toSet == (kingPiece validatedDirectionsFrom position).keys)
  }

  "any other Piece" should "move as the assigned Role specifies" in {
    for(role <- List(Queen, Bishop, Knight, Rook);
        color <- List(White, Black);
        position <- ChessBoardPosition.getPositionsFromSymbols(List('f6, 'a1, 'd8))) {
      val piece: ChessPiece = ChessPiece(color, role)
      assert((role directionsFrom position).toSet == (piece validatedDirectionsFrom position).keys)
    }
  }

  "a Piece during a game" should "have its own path flagged according to the board state" in {
    val collector: Map[BoardPosition, PathControl] = Map.empty
    val results = (gameState.board pieceAt 'c4 get).validatedDirectionsFrom('c4)
      .foldLeft(collector)((acc, entry) => acc ++ entry._1.map(pos => (pos, entry._2._1.applyQuery(gameState, pos))))
    val expected: Map[BoardPosition, PathControl] = Map(
      ChessBoardPosition('b5) -> Continue,
      ChessBoardPosition('a6) -> Continue,
      ChessBoardPosition('d5) -> IncludeAndStop,
      ChessBoardPosition('e6) -> Continue,
      ChessBoardPosition('f7) -> IncludeAndStop,
      ChessBoardPosition('g8) -> IncludeAndStop,
      ChessBoardPosition('d3) -> Continue,
      ChessBoardPosition('e2) -> Continue,
      ChessBoardPosition('f1) -> Continue,
      ChessBoardPosition('b3) -> Continue,
      ChessBoardPosition('a2) -> Stop
    )
    assert(results.keys.forall(key => expected(key) == results(key)))
  }

}