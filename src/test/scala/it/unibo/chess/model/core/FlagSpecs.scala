package it.unibo.chess.model.core

import it.unibo.chess.GameExamples.advancedGameState
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FlagSpecs extends FunSpec {

  describe("Game flags should be updated with current game state") {

    it("should be initialized with default parameters") {
      val game = ChessGame(players = Map(White -> ChessPlayer("player1"), Black -> ChessPlayer("player2")))
      assert(game.flags.castlingAvailability == ChessFlagsUtilities.castlingByBoolean(true))
      assert(game.flags.enPassantTarget.isEmpty)
      assert(game.flags.halfMove == 0)
      assert(game.flags.fullMove == 1)
    }

    it("should update correctly game flags") {
      val game = advancedGameState
      val castlingMap: Map[(ChessColor, BoardPosition), Boolean] = ChessFlagsUtilities.castlingByBoolean(true).foldLeft(Map.empty[(ChessColor, BoardPosition), Boolean])
      {(acc, next) => acc + (if(next._1._1 == Black) next._1 -> false else next)}
        Map(
          (White, ChessBoardPosition('c1)) -> true,
          (White, ChessBoardPosition('g1)) -> true,
          (Black, ChessBoardPosition('c8)) -> false,
          (Black, ChessBoardPosition('g8)) -> false)
      assert(game.flags.castlingAvailability == castlingMap)
      assert(game.flags.enPassantTarget.isEmpty)
      assert(game.flags.halfMove == 2)
      assert(game.flags.fullMove == 4)
      val nextGame = game.move('g2, 'g4)
      assert(nextGame.flags.castlingAvailability == castlingMap)
      assert(nextGame.flags.enPassantTarget.contains(ChessBoardPosition('g3)))
      assert(nextGame.flags.halfMove == 0)
      assert(nextGame.flags.fullMove == 4)
      val otherGame = nextGame.move('h7, 'h5).move('g4, 'g5)
        .move('h8, 'h7)
      assert(otherGame.flags.castlingAvailability == castlingMap)
      assert(otherGame.flags.enPassantTarget.isEmpty)
      assert(otherGame.flags.halfMove == 1)
      assert(otherGame.flags.fullMove == 6)
      val rightRookGame = otherGame.move('h1, 'g1)
      assert(rightRookGame.flags.castlingAvailability == Map(
        (White, ChessBoardPosition('c1)) -> true,
        (White, ChessBoardPosition('g1)) -> false,
        (Black, ChessBoardPosition('c8)) -> false,
        (Black, ChessBoardPosition('g8)) -> false))
      assert(rightRookGame.flags.enPassantTarget.isEmpty)
      assert(rightRookGame.flags.halfMove == 2)
      assert(rightRookGame.flags.fullMove == 6)
      val captureGame = rightRookGame.move('h5, 'h4).move('c4, 'd5)
      assert(captureGame.flags.castlingAvailability == Map(
        (White, ChessBoardPosition('c1)) -> true,
        (White, ChessBoardPosition('g1)) -> false,
        (Black, ChessBoardPosition('c8)) -> false,
        (Black, ChessBoardPosition('g8)) -> false))
      assert(captureGame.flags.enPassantTarget.isEmpty)
      assert(captureGame.flags.halfMove == 0)
      assert(captureGame.flags.fullMove == 7)
      val beforePromotionGame = captureGame
        .move('a7, 'a6).move('g5, 'g6)
        .move('a6, 'a5).move('g6, 'h7)
        .move('a5, 'a4)
      assert(!beforePromotionGame.flags.promotionAvailability)
      val promotionGame = beforePromotionGame.move('h7, 'g8)
      assert(promotionGame.flags.promotionAvailability)
      val selectedPromotionRoleGame = promotionGame.promote('g8, ChessRoles.Queen)
      assert(!selectedPromotionRoleGame.flags.promotionAvailability)
      val afterPromotionGame = selectedPromotionRoleGame.move('a4, 'a3)
      assert(!afterPromotionGame.flags.promotionAvailability)
    }
  }

}
