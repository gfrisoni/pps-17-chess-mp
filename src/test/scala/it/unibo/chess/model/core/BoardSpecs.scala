package it.unibo.chess.model.core

import it.unibo.chess.exceptions.Board.BoardException
import it.unibo.chess.exceptions.Promotion.PromotionException
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.ChessRoles._
import it.unibo.chess.model.core.position.{BoardPosition, ChessBoardPosition}
import org.junit.runner.RunWith
import org.scalatest.FunSpec
import org.scalatest.junit.JUnitRunner

import scala.language.reflectiveCalls

@RunWith(classOf[JUnitRunner])
class BoardSpecs extends FunSpec {

  val board = ChessBoard()
  describe("A Board") {

    it("should not have ChessPieces by default") {assert(board.pieces == Map.empty)}

    it("should not have any taken ChessPiece by default") {assert(board.taken == List.empty)}

    it("should generate a new Board with a given ChessPiece at the given position on it") {
      val startingBoard: ChessBoard = ChessBoard()
      val piece: ChessPiece = ChessPiece(Black, Bishop)
      assert((startingBoard put piece at 'c4).pieces == Map(ChessBoardPosition('c4) -> piece))
    }

    it("should reset to the chess starting position") {
      val gameReadyBoard: ChessBoard = board.reset()
      assert(gameReadyBoard.pieces == Map(
        ChessBoardPosition('a1) -> ChessPiece(White, Rook),
        ChessBoardPosition('b1) -> ChessPiece(White, Knight),
        ChessBoardPosition('c1) -> ChessPiece(White, Bishop),
        ChessBoardPosition('d1) -> ChessPiece(White, Queen),
        ChessBoardPosition('e1) -> ChessPiece(White, King(White)),
        ChessBoardPosition('f1) -> ChessPiece(White, Bishop),
        ChessBoardPosition('g1) -> ChessPiece(White, Knight),
        ChessBoardPosition('h1) -> ChessPiece(White, Rook),
        ChessBoardPosition('a2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('b2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('c2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('d2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('e2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('f2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('g2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('h2) -> ChessPiece(White, Pawn(White)),
        ChessBoardPosition('a7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('b7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('c7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('d7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('e7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('f7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('g7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('h7) -> ChessPiece(Black, Pawn(Black)),
        ChessBoardPosition('a8) -> ChessPiece(Black, Rook),
        ChessBoardPosition('b8) -> ChessPiece(Black, Knight),
        ChessBoardPosition('c8) -> ChessPiece(Black, Bishop),
        ChessBoardPosition('d8) -> ChessPiece(Black, Queen),
        ChessBoardPosition('e8) -> ChessPiece(Black, King(Black)),
        ChessBoardPosition('f8) -> ChessPiece(Black, Bishop),
        ChessBoardPosition('g8) -> ChessPiece(Black, Knight),
        ChessBoardPosition('h8) -> ChessPiece(Black, Rook)
      ))
    }

    it("should update the structures accordingly") {
      val piece: ChessPiece = ChessPiece(White, Queen)
      var gameBoard: ChessBoard = board put piece at 'f8
      gameBoard = gameBoard take 'f8
      assert(gameBoard.pieces == Map.empty)
      assert(gameBoard.taken == List(piece))
    }

    it("should allow moving ChessPieces on an empty position") {
      val piece: ChessPiece = ChessPiece(Black, Rook)
      val origin = 'f8
      val destination = 'f3
      var gameBoard: ChessBoard = board put piece at origin
      gameBoard = gameBoard move ChessBoardPosition(origin) to destination
      assert(gameBoard.pieces == Map(ChessBoardPosition(destination) -> piece))
    }

    it("should deny moving ChessPieces on an occupied position") {
      val piece1: ChessPiece = ChessPiece(Black, Rook)
      val piece2: ChessPiece = ChessPiece(Black, Knight)
      val origin = 'f8
      val destination = 'f3
      var gameBoard: ChessBoard = board put piece1 at origin
      gameBoard = gameBoard put piece2 at destination
      assertThrows[BoardException](gameBoard move origin to destination)
    }

    it("should deny moving from an empty position") {
      assertThrows[BoardException](board move 'c3 to 'h1)
    }

    it("should return ChessPieces at a given position") {
      val gameBoard = board.reset()
      assert((gameBoard pieceAt 'a2).get == ChessPiece(White, Pawn(White)))
      assert((gameBoard pieceAt 'a1).get == ChessPiece(White, Rook))
      assert((gameBoard pieceAt 'd8).get == ChessPiece(Black, Queen))
      assert((gameBoard pieceAt 'g7).get == ChessPiece(Black, Pawn(Black)))
    }

    it("should return an exception if a ChessPiece is not present") {
      val gameBoard = board.reset()
      assertThrows[NoSuchElementException]((gameBoard pieceAt 'd4).get)
      assertThrows[NoSuchElementException]((gameBoard pieceAt 'c5).get)
      assertThrows[NoSuchElementException]((gameBoard pieceAt 'd6).get)
      assertThrows[NoSuchElementException]((gameBoard pieceAt 'h3).get)
    }

    it("should find both King's positions") {
      val kingPositions: Map[ChessColor , BoardPosition] = Map(Black -> 'e8, White -> 'e1)
      assert(board.reset().kingPos == kingPositions)
    }

    val promotionBoard = ChessBoard(Map(ChessBoardPosition('e8) -> ChessPiece(White, Pawn(White))),Nil)
    it("should promote a Pawn") {
      assert((promotionBoard.promote('e8) to Rook).pieces == Map(ChessBoardPosition('e8) -> ChessPiece(White, Rook)),Nil)
    }

    describe("should catch invalid promotion attempts if it tries to promote") {
      it("to a King") {
        assertThrows[PromotionException](promotionBoard.promote('e8) to King(White))
      }

      val invalidPromotionBoard1 = ChessBoard(Map(ChessBoardPosition('e7) -> ChessPiece(White, Pawn(White))), Nil)
      it("on a wrong line") {
        assertThrows[PromotionException](invalidPromotionBoard1.promote('e7) to Bishop)
      }

      it("from an empty position") {
        assertThrows[PromotionException](invalidPromotionBoard1.promote('e8) to Bishop)
      }

      it("a piece that isn't a Pawn") {
        val invalidPromotionBoard2 = ChessBoard(Map(ChessBoardPosition('e8) -> ChessPiece(White, Rook)), Nil)
        assertThrows[PromotionException](invalidPromotionBoard2.promote('e8) to Bishop)
      }

      it("should print a nice toString") {
        val boardString: String = ChessBoard().reset().toString
        assert(boardString.lines.length == 8)
        boardString.lines.toIndexedSeq.foreach(l => {
          assert(l.head.isDigit)
          assert(l.length == 10)
        })
      }
    }

  }

}
