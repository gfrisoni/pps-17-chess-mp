package it.unibo.chess.controller.preferences

import it.unibo.chess.view.fx.utilities.BoardStyles
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class PreferencesHandlerSpecs extends FunSpec with Matchers with BeforeAndAfterAll {

  describe("A user preferences handler") {

    it("should be able to resets them and retrieve default values when asked") {
      PreferencesHandler.reset()
      PreferencesHandler.getLegalMovesPreference should be(true)
      PreferencesHandler.getLastMovePreference should be(true)
      PreferencesHandler.getStylePreference should be(BoardStyles.Default) // TODO: change with classic style enumeration
    }

    it("should be able to store a legal moves highlighting preference") {
      PreferencesHandler.reset()
      PreferencesHandler.setLegalMovesPreference(false)
      PreferencesHandler.getLegalMovesPreference should be(false)
    }

    it("should be able to store a last move highlighting preference") {
      PreferencesHandler.reset()
      PreferencesHandler.setLastMovePreference(false)
      PreferencesHandler.getLastMovePreference should be(false)
    }

    it("should be able to store a style preference") {
      PreferencesHandler.reset()
      PreferencesHandler.setStylePreference(BoardStyles.Realistic) // TODO: change with classic style enumeration
      PreferencesHandler.getStylePreference should be(BoardStyles.Realistic)
    }

  }

  override def afterAll(): Unit = {
    // Resets all preferences at test ending
    PreferencesHandler.reset()
  }

}
