package it.unibo.chess.controller.database

import com.github.nscala_time.time.Imports.DateTime
import com.github.simplyscala.MongoEmbedDatabase
import it.unibo.chess.controller.database.Helpers._
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.model.core.position.ChessBoardPosition.SymbolConverter._
import it.unibo.chess.model.core.{Black, ChessGame, White}
import it.unibo.chess.model.fen.FenParser
import it.unibo.chess.model.modes.ChessModes
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

import scala.concurrent.duration._

/**
  * This test class uses different mongodb ports in order to avoid troubles during parallel execution
  * as indicated at [[https://github.com/SimplyScala/scalatest-embedmongo Scalatest Embed Mongo Guide]].
  */
@RunWith(classOf[JUnitRunner])
class ChessGameDAOSpecs extends FunSpec with Matchers with MongoEmbedDatabase {

  val exampleOldestGame: ChessGame = ChessGame.copy(FenParser
    .fromFen("6k1/5ppp/8/8/8/8/8/R3K3 w KQkq - 0 1")
    .move('a1, 'a8))(newDateTime = (new DateTime).withYear(2018).withMonthOfYear(9).withDayOfMonth(15))
  val exampleNewerGame: ChessGame = ChessGame.copy(FenParser
    .fromFen("6k1/5ppp/8/8/8/8/8/R3K3 w KQkq - 0 1")
    .move('a1, 'a8))(newDateTime = (new DateTime).withYear(2018).withMonthOfYear(9).withDayOfMonth(26))
  val gameClock: ChessClock = ChessClock(
    ChessModes.SuddenDeath,
    Map(White -> 10.minutes, Black -> 10.minutes)
  )

  describe("A ChessGameDAO") {

    it("should be able to save a game into the database") {
      withEmbedMongoFixture(LocalConnectors.config1.port) { _ =>
        val (collection, functions) = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config1)
        collection.drop().headResult()
        functions.saveGame(exampleNewerGame, gameClock).headResult()
        collection.countDocuments().headResult() should be(1)
      }
    }

    it("should be able to count the number of saved games into the database") {
      withEmbedMongoFixture(LocalConnectors.config2.port) { _ =>
        val (collection, functions) = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config2)
        collection.drop().headResult()
        functions.gamesSize.headResult() should be(0)
      }
    }

    it("should be able to retrieve all saved games from the database") {
      withEmbedMongoFixture(LocalConnectors.config3.port) { _ =>
        val (collection, functions) = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config3)
        collection.drop().headResult()
        functions.saveGame(exampleNewerGame, gameClock).headResult()
        functions.saveGame(exampleOldestGame, gameClock).headResult()
        val allGames: Seq[ChessGameMongo] = functions.findAllGames().results()
        allGames.lengthCompare(2) should be(0)
        allGames.map(g => g.dateTime) should be(Seq("2018.09.26", "2018.09.15"))
      }
    }

    it("should be able to update the number of views of a specific game to database") {
      withEmbedMongoFixture(LocalConnectors.config4.port) { _ =>
        val (collection, functions) = LocalConnectors.getCollectionAndFunctions(LocalConnectors.config4)
        collection.drop().headResult()
        functions.saveGame(exampleNewerGame, gameClock).headResult()
        val savedGame = functions.findAllGames().results()
        savedGame.size should be(1)
        functions.incrementViews(savedGame.last._id).headResult()
        functions.findAllGames().results().last.views should be(1)
        functions.incrementViews(savedGame.last._id).headResult()
        functions.findAllGames().results().last.views should be(2)
      }
    }

  }

}
