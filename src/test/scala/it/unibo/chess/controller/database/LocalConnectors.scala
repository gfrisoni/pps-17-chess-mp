package it.unibo.chess.controller.database

import org.mongodb.scala.{MongoClient, MongoCollection}

/**
  * Provides useful connection configurations to local mongo databases for testing purposes.
  */
object LocalConnectors {

  case class MongoConfig(port: Int)

  /**
    * Creates and provides the mongo collection and the custom functions for the ChessGameDAO
    * configured through the specified mongo configuration.
    *
    * @param mongoConfig the configuration to use
    * @return the mongo collection and the custom ChessGameDAO functions
    */
  def getCollectionAndFunctions(mongoConfig: MongoConfig): (MongoCollection[ChessGameMongo], ChessGameDatabaseFunctions) = {
    val database = ChessMultiplayerDatabase(MongoClient("mongodb://localhost:" + mongoConfig.port))
    (database.ChessGameDAO.collection, ChessGameDatabaseFunctions(database))
  }

  val config1 = MongoConfig(12345)
  val config2 = MongoConfig(12346)
  val config3 = MongoConfig(12347)
  val config4 = MongoConfig(12348)

}