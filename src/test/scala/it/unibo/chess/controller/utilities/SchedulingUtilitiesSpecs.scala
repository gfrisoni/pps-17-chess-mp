package it.unibo.chess.controller.utilities

import java.util.concurrent.atomic.AtomicInteger

import com.miguno.akka.testing.VirtualTime
import it.unibo.chess.controller.utilities.SchedulingUtilities.AdvancedScheduler
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, GivenWhenThen, Matchers}

import scala.concurrent.duration._

@RunWith(classOf[JUnitRunner])
class SchedulingUtilitiesSpecs extends FunSpec with Matchers with GivenWhenThen {

  describe("A task scheduling library") {

    it("should run one-time task once") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("an advanced scheduler")
      val advancedScheduler = AdvancedScheduler(time.scheduler)

      When("I schedule a one-time task")
      val counter = new AtomicInteger(0)
      advancedScheduler.doOnce(counter.getAndIncrement())(5.millis)

      Then("the task should not run before its delay")
      time.advance(4.millis)
      counter.get should be(0)
      And("the task should run at the time of its delay")
      time.advance(1.millis)
      counter.get should be(1)
      And("the task should not run again")
      time.advance(10000.millis)
      counter.get should be(1)
    }

    it("should run a regularly task multiple times") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("an advanced scheduler")
      val advancedScheduler = AdvancedScheduler(time.scheduler)

      When("I schedule a regularly task (initial delay included)")
      val counter = new AtomicInteger(0)
      val period: FiniteDuration = 10.millis
      advancedScheduler.doRegularly(counter.getAndIncrement())(period)

      Then("the task should not run before its initial delay")
      time.advance(4.millis)
      counter.get should be(0)
      And("it should run at the time of its initial delay (run #1)")
      time.advance(6.millis)
      counter.get should be(1)
      And("it should not run again before its next interval")
      time.advance(9.millis)
      counter.get should be(1)
      And("it should run again at its next interval (run #2)")
      time.advance(1.millis)
      counter.get should be(2)
      And("it should not run again before its next interval")
      time.advance(9.millis)
      counter.get should be(2)
      And("it should run again at its next interval (run #3)")
      time.advance(1.millis)
      counter.get should be(3)
      And("it should have run 103 times after the initial delay and 102 intervals")
      time.advance(period * 100)
      counter.get should be(103)
    }

    it("should run a regularly task multiple times until a certain condition") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("an advanced scheduler")
      val advancedScheduler = AdvancedScheduler(time.scheduler)

      When("I schedule a regularly task with while condition and no ending task")
      val counter = new AtomicInteger(0)
      val period = 10.millis
      var whileCondition = true
      advancedScheduler.doWhile(counter.getAndIncrement(), whileCondition, None)(period)

      Then("the task should run instantly")
      time.advance(4.millis)
      counter.get should be(1)
      And("it should run again at the time of its initial delay (run #2)")
      time.advance(6.millis)
      counter.get should be(2)
      And("it should not run again before its next interval")
      time.advance(9.millis)
      counter.get should be(2)
      And("it should run again at its next interval (run #3)")
      time.advance(1.millis)
      counter.get should be(3)
      And("it should not run again before its next interval")
      time.advance(9.millis)
      counter.get should be(3)
      And("it should run again at its next interval (run #4)")
      time.advance(1.millis)
      counter.get should be(4)
      And("it should not run again if the while condition becomes false")
      time.advance(2.millis)
      whileCondition = false
      time.advance(8.millis)
      counter.get should be(4)
      And("it should no longer be executed after the while condition occurs")
      time.advance(period * 100)
      counter.get should be(4)

      When("I schedule a regularly task with while condition and an ending task")
      counter.set(0)
      whileCondition = true
      advancedScheduler.doWhile(counter.getAndIncrement(), whileCondition, Some(() => counter.set(0)))(period)

      Then("the task should run instantly")
      time.advance(4.millis)
      counter.get should be(1)
      And("it should run again at the time of its initial delay (run #2)")
      time.advance(6.millis)
      counter.get should be(2)
      And("it should not run again before its next interval")
      time.advance(9.millis)
      counter.get should be(2)
      And("it should run again at its next interval (run #3)")
      time.advance(1.millis)
      counter.get should be(3)
      And("it should run the ending task if the while condition becomes false")
      time.advance(2.millis)
      whileCondition = false
      time.advance(8.millis)
      counter.get should be(0)
      And("it should no longer be executed after the while condition occurs")
      time.advance(period * 100)
      counter.get should be(0)
    }

    it("should not run a cancelled task") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("an advanced scheduler")
      val advancedScheduler = AdvancedScheduler(time.scheduler)

      When("I schedule a one-time task")
      val counter = new AtomicInteger(0)
      val delayLength = 5L
      val delayUnit = MILLISECONDS
      val scheduledIncrement = advancedScheduler.doRegularly(counter.getAndIncrement())(delayLength, delayUnit)
      And("I cancel the task before its execution time")
      scheduledIncrement.cancel()
      time.advance(FiniteDuration(delayLength, delayUnit))

      Then("the task should not run")
      counter.get should be(0)
    }

    it("should not run a regularly task after it was cancelled") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("an advanced scheduler")
      val advancedScheduler = AdvancedScheduler(time.scheduler)

      When("I schedule a regularly task")
      val counter = new AtomicInteger(0)
      val delayLength = 5L
      val delayUnit = MILLISECONDS
      val scheduledIncrement = advancedScheduler.doRegularly(counter.getAndIncrement())(delayLength, delayUnit)
      And("I advance the time so that task is executed once")
      time.advance(FiniteDuration(delayLength, delayUnit))
      counter.get should be(1)
      And("I cancel the task and advance the time further")
      scheduledIncrement.cancel()
      time.advance(FiniteDuration(delayLength, delayUnit))

      Then("the task should not run anymore")
      counter.get should be(1)
    }

  }

}
