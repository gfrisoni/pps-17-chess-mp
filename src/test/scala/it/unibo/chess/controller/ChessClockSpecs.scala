package it.unibo.chess.controller

import java.util.concurrent.atomic.AtomicInteger

import com.miguno.akka.testing.VirtualTime
import it.unibo.chess.controller.scheduling.ChessClock
import it.unibo.chess.controller.traits.scheduling.ChessClockObserver
import it.unibo.chess.controller.utilities.SchedulingUtilities.AdvancedScheduler
import it.unibo.chess.model.core.{Black, ChessColor, White}
import it.unibo.chess.model.modes.ChessModes.{Compensation, Hourglass, SuddenDeath}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, GivenWhenThen, Matchers}
import it.unibo.chess.controller.scheduling.TimeHandler.RelTime.toRelTime
import it.unibo.chess.exceptions.Timer.Types.TimerException

import scala.concurrent.duration._

@RunWith(classOf[JUnitRunner])
class ChessClockSpecs extends FunSpec with Matchers with GivenWhenThen {

  describe("A chess game timer") {

    it("should properly count turn seconds of a player") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.seconds.rel, Black -> 10.seconds.rel),
        AdvancedScheduler(time.scheduler)
      )
      And("a generic timer observer with a tick counter logic")
      val counter = new AtomicInteger(0)
      var timeoutFlag = false
      val observer: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = counter.getAndIncrement()
        override def onTimeout(loser: ChessColor): Unit = timeoutFlag = true
      }
      gameTimer.addObserver(observer)

      When("I start the timer with a black pressure")
      gameTimer.blackPressure()

      Then("the tick event should be called after 1 second")
      time.advance(1.seconds.rel)
      counter.get() should be(1)
      timeoutFlag should be(false)
      And("it should be called after 2 seconds")
      time.advance(1.seconds.rel)
      counter.get() should be(2)
      timeoutFlag should be(false)
      And("it should be called after 3 seconds")
      time.advance(1.seconds.rel)
      counter.get() should be(3)
      timeoutFlag should be(false)
      And("the timeout should be called after the player time erasing")
      time.advance(7.seconds.rel)
      timeoutFlag should be(true)
      And("the tick event should not be called anymore after timeout")
      time.advance(10.seconds.rel)
      counter.get() should be(9)
    }

    it("should be able to correctly switch players' timers") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.minutes, Black -> 10.minutes),
        AdvancedScheduler(time.scheduler)
      )
      And("a generic timer observer")
      var currentTickTimers: Map[ChessColor, FiniteDuration] = Map.empty
      val observer: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = currentTickTimers = timers
        override def onTimeout(loser: ChessColor): Unit = {}
      }
      gameTimer.addObserver(observer)

      When("I start the timer with a black pressure")
      gameTimer.blackPressure()

      Then("the white player timer should decrease of 1 second after the first tick")
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 59.seconds)
      currentTickTimers(Black) should be(10.minutes)
      Then("the white player timer should decrease of 2 seconds after the second tick")
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds)
      currentTickTimers(Black) should be(10.minutes)
      Then("the white player timer should decrease of 3 second after the third tick")
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 57.seconds)
      currentTickTimers(Black) should be(10.minutes)

      When("I switch the timer with a white pressure")
      gameTimer.whitePressure()

      Then("the black player timer should decrease of 1 second after the first tick and the white player timer should stop")
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 57.seconds)
      currentTickTimers(Black) should be(9.minutes + 59.seconds)
      Then("the black player timer should decrease of 1 minute after sixty ticks")
      time.advance(1.minutes)
      currentTickTimers(White) should be(9.minutes + 57.seconds)
      currentTickTimers(Black) should be(8.minutes + 59.seconds)

      When("I switch the timer with a black pressure again")
      gameTimer.blackPressure()

      Then("the white player timer should resume and the the black one should stop")
      time.advance(2.minutes + 30.seconds)
      currentTickTimers(White) should be(7.minutes + 27.seconds)
      currentTickTimers(Black) should be(8.minutes + 59.seconds)
    }

    it("should be able to handle different update strategies") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a generic timer observer")
      var currentTickTimers: Map[ChessColor, FiniteDuration] = Map.empty
      val observer: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = currentTickTimers = timers
        override def onTimeout(loser: ChessColor): Unit = {}
      }

      When("I select SuddenDeath strategy")
      val suddenDeathGameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.minutes, Black -> 10.minutes),
        AdvancedScheduler(time.scheduler)
      )
      suddenDeathGameTimer.addObserver(observer)

      Then("the timer of the current player should decrease and the opposite one should remain stopped")
      suddenDeathGameTimer.blackPressure()
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 59.seconds)
      currentTickTimers(Black) should be(10.minutes)
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds)
      currentTickTimers(Black) should be(10.minutes)
      suddenDeathGameTimer.whitePressure()
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds)
      currentTickTimers(Black) should be(9.minutes + 59.seconds)
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds)
      currentTickTimers(Black) should be(9.minutes + 58.seconds)
      suddenDeathGameTimer.stop()

      When("I select Hourglass strategy")
      val hourglassGameTimer: ChessClock = ChessClock.copy(suddenDeathGameTimer)(newChessMode = Hourglass)
      hourglassGameTimer.addObserver(observer)

      Then("the timer of the current player should decrease and the opposite one should increase")
      hourglassGameTimer.blackPressure()
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 59.seconds)
      currentTickTimers(Black) should be(10.minutes + 1.seconds)
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds)
      currentTickTimers(Black) should be(10.minutes + 2.seconds)
      hourglassGameTimer.whitePressure()
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 59.seconds)
      currentTickTimers(Black) should be(10.minutes + 1.seconds)
      time.advance(1.seconds)
      currentTickTimers(White) should be(10.minutes)
      currentTickTimers(Black) should be(10.minutes)
      hourglassGameTimer.stop()

      When("I select Compensation strategy")
      val bonusTime: FiniteDuration = 10.seconds
      val compensationGameTimer: ChessClock = ChessClock.copy(suddenDeathGameTimer)(newChessMode = Compensation(bonusTime))
      compensationGameTimer.addObserver(observer)

      Then("a bonus time should be added to the player when the timer switches on his turn")
      compensationGameTimer.blackPressure()
      compensationGameTimer.getTimerValues(White) should be(10.minutes + bonusTime)
      compensationGameTimer.getTimerValues(Black) should be(10.minutes)
      And("the timer of the current player should decrease and the opposite one should remain stopped")
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 59.seconds + bonusTime)
      currentTickTimers(Black) should be(10.minutes)
      time.advance(1.seconds)
      currentTickTimers(White) should be(9.minutes + 58.seconds + bonusTime)
      currentTickTimers(Black) should be(10.minutes)
      time.advance(1.minutes + 50.seconds)
      currentTickTimers(White) should be(8.minutes + 8.seconds + bonusTime)
      currentTickTimers(Black) should be(10.minutes)
      compensationGameTimer.whitePressure()
      time.advance(1.seconds)
      currentTickTimers(White) should be(8.minutes + 8.seconds + bonusTime)
      currentTickTimers(Black) should be(9.minutes + 59.seconds + bonusTime)
      time.advance(1.seconds)
      currentTickTimers(White) should be(8.minutes + 8.seconds + bonusTime)
      currentTickTimers(Black) should be(9.minutes + 58.seconds + bonusTime)
    }

    it("should be stoppable") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.seconds, Black -> 10.seconds),
        AdvancedScheduler(time.scheduler)
      )
      And("a generic timer observer with a tick counter logic")
      val counter = new AtomicInteger(0)
      val observer: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = counter.getAndIncrement()
        override def onTimeout(loser: ChessColor): Unit = {}
      }
      gameTimer.addObserver(observer)

      When("I start the timer with a black pressure")
      gameTimer.blackPressure()

      Then("the tick event should be called after 1 second")
      time.advance(1.seconds.rel)
      counter.get() should be(1)
      And("it should be called after 2 seconds")
      time.advance(1.seconds.rel)
      counter.get() should be(2)
      And("it should be called after 3 seconds")
      time.advance(1.seconds.rel)
      counter.get() should be(3)
      And("it should not be called anymore after being stopped")
      gameTimer.stop()
      time.advance(10.seconds.rel)
      counter.get() should be(3)
    }

    it("should handle more observers") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.minutes, Black -> 10.minutes),
        AdvancedScheduler(time.scheduler)
      )
      And("a two timer observers with a tick counter logic")
      val counter1 = new AtomicInteger(0)
      val counter2 = new AtomicInteger(0)
      val observer1: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = counter1.getAndIncrement()
        override def onTimeout(loser: ChessColor): Unit = {}
      }
      val observer2: ChessClockObserver = new ChessClockObserver {
        override def onTick(timers: Map[ChessColor, FiniteDuration]): Unit = counter2.getAndIncrement()
        override def onTimeout(loser: ChessColor): Unit = {}
      }
      gameTimer.addObserver(observer1)
      gameTimer.addObserver(observer2)

      When("I start the timer with a black pressure")
      gameTimer.blackPressure()

      Then("Both observers should be notified of a tick event after 1 second")
      time.advance(1.seconds.rel)
      counter1.get() should be(1)
      counter2.get() should be(1)
      And("Both should be notified of a tick event after 2 seconds")
      time.advance(1.seconds.rel)
      counter1.get() should be(2)
      counter2.get() should be(2)
      And("it should have run 103 times after the initial delay and 102 intervals")
      time.advance(100.seconds.rel)
      counter1.get should be(102)
      counter1.get should be(102)
    }

    it("should throw an exception if a player press its button color when it is already his turn") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.minutes, Black -> 10.minutes),
        AdvancedScheduler(time.scheduler)
      )

      When("I start the timer with a black pressure")
      gameTimer.blackPressure()

      Then("The timer should throw an exception if I do a black pressure again")
      an [TimerException] should be thrownBy gameTimer.blackPressure()
    }

    it("should throw an exception if white tries to start the timer first") {
      Given("a time with a scheduler")
      val time = new VirtualTime
      And("a game timer with specifics starting values")
      val gameTimer: ChessClock = ChessClock(
        SuddenDeath,
        Map(White -> 10.minutes, Black -> 10.minutes),
        AdvancedScheduler(time.scheduler)
      )

      When("I start the timer with a white pressure")
      Then("The timer should throw an exception as it's white's turn and black should switch the clock")
      an [TimerException] should be thrownBy gameTimer.whitePressure()
    }

  }

}
