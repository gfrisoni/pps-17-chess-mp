package it.unibo.chess.view.dummy

import it.unibo.chess.controller.traits.scenes.game.GameSceneController
import it.unibo.chess.model.core._
import it.unibo.chess.model.core.position.BoardPosition
import it.unibo.chess.model.history.ChessActions.ChessEndingAction
import it.unibo.chess.model.modes.ChessModes.ChessMode
import it.unibo.chess.model.pgn.{PgnFullMove, PgnMoveIndex}
import it.unibo.chess.view.GenericWindow
import it.unibo.chess.view.general.scenes.GameScene
import it.unibo.chess.view.general.views.DrawRequestMode
import org.joda.time.DateTime

import scala.concurrent.duration.FiniteDuration

class DummyView extends GameScene {

  override def init(players: Map[ChessColor, ChessPlayer], sideSize: Int): Unit = observers.foreach(_.onViewLoadCompletion())

  override def setGameEnded(endingAction: ChessEndingAction): Unit = {}

  override val windowManager: GenericWindow = new DummyWindowManager()

  override def updateClock(clock: Map[ChessColor, FiniteDuration]): Unit = {}

  override def updateTakenPieces(chessColor: ChessColor, pieces: Seq[ChessPiece]): Unit = {}

  override def setTurn(chessColor: ChessColor): Unit = {}

  override def setHistory(history: (Map[Int, PgnFullMove], Option[String])): Unit = {}

  override def setCurrentMoveInHistory(moveIndex: Option[PgnMoveIndex]): Unit = {}

  override def setPlayModeEnabled(isEnabled: Boolean): Unit = {}

  override def setStopEnabled(isEnabled: Boolean): Unit = {}

  override def setWhiteDrawRequestEnabled(isEnabled: Boolean): Unit = {}

  override def setBlackDrawRequestEnabled(isEnabled: Boolean): Unit = {}

  override def setDrawRequestMode(drawRequestMode: DrawRequestMode.Value, color: ChessColor): Unit = {}

  override def drawBoard(pieces: Map[BoardPosition, ChessPiece],
                         lastMove: Option[(BoardPosition, BoardPosition)]): Unit = {}

  override def setSquaresToHighlighted(boardPositions: Set[BoardPosition]): Unit = {}

  override def setSquareSelected(boardPosition: BoardPosition): Unit = {}

  override def enableBoard(): Unit = {}

  override def disableBoard(): Unit = {}

  override def setKingCheck(kingPosition: BoardPosition): Unit = {}

  override def setFirstGameStepEnabled(isEnabled: Boolean): Unit = {}

  override def setPreviousGameStepEnabled(isEnabled: Boolean): Unit = {}

  override def setPlayEnabled(isEnabled: Boolean): Unit = {}

  override def setNextGameStepEnabled(isEnabled: Boolean): Unit = {}

  override def setLastGameStepEnabled(isEnabled: Boolean): Unit = {}

  override def showDownloadInfo(fen: String, pgn: String, dateTime: DateTime): Unit = {}

  override def enablePlayerWaiting(chessColor: ChessColor): Unit = {}

  override def disablePlayerWaiting(): Unit = {}

  override def setIP(ip: String): Unit = {}

  override def setPort(port: String): Unit = {}

  override def getIP: String = ""

  override def getPort: Int = 0

  override def setOnWaitingForSave(): Unit = {}

  override def setPlayable(clock: Map[ChessColor, FiniteDuration], gameMode: ChessMode, forPlayer: Option[(ChessColor, String)]): Unit = {}

  override def enablePromotionSelection(boardPosition: BoardPosition, player: ChessColor, promotableRoles: Seq[ChessRoles.ChessRole]): Unit = {}
}

object DummyView {

  /**
    * Setup method for a dummy view that does nothing.
    *
    * @param controller the used controller
    * @param players the players on the game
    * @tparam C a generic gameSceneController
    */
  def setup[C <: GameSceneController](controller: C, players: Map[ChessColor, ChessPlayer]): Unit = {
    val view = new DummyView()
    controller.setView(view)
    view.addObserver(controller)
    view.init(players, 8)
  }

}
