package it.unibo.chess.view

import it.unibo.chess.view.fx.utilities.Animations
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent
import javafx.application.Platform
import javafx.stage.Stage
import org.junit.Test
import org.testfx.framework.junit.ApplicationTest

class MainViewTest extends ApplicationTest {
  private val TimeToCompleteAnimation = Animations.Fade.FADE_DURATION + 100
  var view: GenericWindow = _
  val title: String = "Chess MP"

  override def start(stage: Stage): Unit = {
    Platform.runLater(() => {
      stage.setAlwaysOnTop(true)

      view = FXWindow(stage)
      view.setTitle(title)
      view.setScene(new Intent(SceneType.MainScene))
      view.showView()
    })

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldBeVisible(): Unit = {
    assert(view.isShowing, "View is not visible")
    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldHaveMainTitle(): Unit = {
    Platform.runLater(() => {
      assert(title == view.getTitle, "Title is not set to '" + title + "'")
    })

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldMainMenuBeVisible(): Unit = {
    Platform.runLater(() => {
      assert(view.getScene != null)
      assert(view.getScene.get == SceneType.MainScene)
    })

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldHaveMenuButtons(): Unit = {
    Platform.runLater(() => {
      val localButton = lookup("#startLocalGameBtn")
      assert(localButton != null, "There is no button to start local game")

      val multiplayerButton = lookup("#startMultiPlayerGameBtn")
      assert(multiplayerButton != null, "There is no button to start multiplayer game")

      val tvModeButton = lookup("#tvModeBtn")
      assert(tvModeButton != null, "There is no button to open the TV Mode")

      val settingsButton = lookup("#settingsBtn")
      assert(settingsButton != null, "There is no button to open the settings")
    })

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldOpenLocalGameCreatorScene(): Unit = {
    clickOn("#startLocalGameBtn")
    sleep(TimeToCompleteAnimation)
    assert(view.getScene.get == SceneType.LocalGameCreatorScene)
    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldOpenMultiplayerGameScene(): Unit = {
    clickOn("#startMultiPlayerGameBtn")
    sleep(TimeToCompleteAnimation)
    assert(view.getScene.get == SceneType.MultiPlayerGameCreatorScene)
    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldOpenSettingsScene(): Unit = {
    clickOn("#settingsBtn")
    sleep(TimeToCompleteAnimation)
    assert(view.getScene.get == SceneType.SettingsScene)
    sleep(TimeToCompleteAnimation)
  }

}