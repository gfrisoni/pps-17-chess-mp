package it.unibo.chess.view

import it.unibo.chess.view.fx.utilities.Animations
import it.unibo.chess.view.general.SceneType
import it.unibo.chess.view.general.intents.Intent
import javafx.application.Platform
import javafx.scene.control.{Button, ComboBox, TextField}
import javafx.stage.Stage
import org.junit.Test
import org.testfx.framework.junit.ApplicationTest

class LocalGameCreationTest extends ApplicationTest {
  private val TimeToCompleteAnimation = Animations.Fade.FADE_DURATION + 100
  var view: GenericWindow = _

  override def start(stage: Stage): Unit = {
    Platform.runLater(() => {
      stage.setAlwaysOnTop(true)

      view = FXWindow(stage)
      view.setScene(new Intent(SceneType.MainScene))
      view.showView()
    })
  }

  @Test def shouldHaveAllInputs(): Unit = {
    goToMenu()
    assert(lookup("#whitePlayerTxt").queryAs(classOf[TextField]) != null, "There is no input for player 1")
    assert(lookup("#blackPlayerTxt").queryAs(classOf[TextField]) != null, "There is no input for player 2")
    assert(lookup("#gameModeCmb").queryAs(classOf[ComboBox[_]]) != null, "There is no input for game mode")
    assert(lookup("#timeCmb").queryAs(classOf[ComboBox[_]]) != null, "There is no input for timer")
    assert(lookup("#backBtn").queryButton() != null, "There is no way to go back to main menu")
    assert(lookup("#startBtn").queryButton() != null, "There is no way to start the game")

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldAskForAllInputsAndStart(): Unit = {
    goToMenu()

    val whitePlayerName: TextField = lookup("#whitePlayerTxt").queryAs(classOf[TextField])
    val blackPlayerName: TextField = lookup("#blackPlayerTxt").queryAs(classOf[TextField])

    //All Empty
    clickOn(lookup("#startBtn").queryAs(classOf[Button]))
    assert(view.getScene.get == SceneType.LocalGameCreatorScene)

    // White's name ok
    whitePlayerName.setText("White name")
    clickOn(lookup("#startBtn").queryAs(classOf[Button]))
    assert(view.getScene.get == SceneType.LocalGameCreatorScene)

    // Black's name equals White's name
    blackPlayerName.setText("White name")
    clickOn(lookup("#startBtn").queryAs(classOf[Button]))
    assert(view.getScene.get == SceneType.LocalGameCreatorScene)

    // Fixing wrong name
    blackPlayerName.setText("Black name")
    clickOn(lookup("#startBtn").queryAs(classOf[Button]))
    assert(view.getScene.get == SceneType.GameScene)

    sleep(TimeToCompleteAnimation)
  }

  @Test def shouldGoBackToMenu(): Unit = {
    goToMenu()
    clickOn("#backBtn")
    sleep(TimeToCompleteAnimation)
    assert(view.getScene.get == SceneType.MainScene)
    sleep(TimeToCompleteAnimation)
  }

  private def goToMenu(): Unit = {
    clickOn("#startLocalGameBtn")
    sleep(TimeToCompleteAnimation)
    assert(view.getScene.get == SceneType.LocalGameCreatorScene)
    sleep(TimeToCompleteAnimation)
  }
}