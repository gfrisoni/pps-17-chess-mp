|  **Oggetto Backlog** | **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Gestione base del FEN  | Adattamento fromFEN | Sofia Rossi | 1 | 1 | 1 |  |  |  |  |
|   | Generazione FEN da stato di gioco | Sofia Rossi | 2 | 2 | 2 | 2 |  |  |  |
|  Modellazione completa di una partita in locale | Scacco | Leonardo Montini | 7 | 3 | 2 |  |  |  |  |
|   | Patta | Leonardo Montini | 1 | 1 | 1 |  |  |  |  |
|   | Promozione | Leonardo Montini | 4 | 4 | 2 |  |  |  |  |
|   | Popolazione history | Giacomo Frisoni | 4 | 2 |  |  |  |  |  |
|   | Flags per arrocco, enpassant e fen | Sofia Rossi | 3 | 3 | 2 |  |  |  |  |
|  Gestione della notazione PGN | Rappresentazione PGN della history | Giacomo Frisoni | 2 | 2 | 2 |  |  |  |  |
|  Gestione base di una partita in locale (interazioni Model-Controller) | Controller di base | Giacomo Frisoni | 4 | 4 | 4 | 2 | 1 |  |  |
|   | Gestione del tempo per i turni dei giocatori | Giacomo Frisoni | 5 | 5 | 5 | 5 | 3 | 1 |  |