#Sprint 2 backlog

|  **Oggetto Backlog** | **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Gestione della history di gioco | Game Step | Giacomo Frisoni | 1 | 1 |  |  |  |  |  |
|   | History in Game | Giacomo Frisoni | 2 | 2 |  |  |  |  |  |
|  Gestione della notazione algebrica | Azioni | Giacomo Frisoni | 2 | 2 | 2 | 3 |  |  |  |
|   | Notazione generica con serializer | Giacomo Frisoni | 5 | 5 | 5 | 2 |  |  |  |
|   | Algebraic Notation con varianti | Giacomo Frisoni | 4 | 4 | 4 | 1 |  |  |  |
|  Gestione base del FEN  | Gestione creazione strutture custom dal FEN | Sofia Rossi | 3 | 3 | 3 | 2 | 1 |  |  |
|  Modellazione completa di una partita in locale | EnPassant | Leonardo Montini | 4 | 2 | 2 | 2 |  |  |  |
|   | Arrocco | Sofia Rossi | 2 | 2 | 2 | 2 | 2 |  |  |
|   | Mosse Legali | Leonardo Montini | 7 | 5 | 3 |  |  |  |  |
|   | Scacco | --- | 7 | 7 | 7 | 7 | 7 | 7 | 7 |
|   | Promozione | --- | 5 | 5 | 5 | 5 | 5 | 5 | 5 |
|  Sviluppo componenti grafici del gioco |  | Marcin Pabich |  |  |  |  |  |  |  |