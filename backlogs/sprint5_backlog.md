
|  **Oggetto Backlog** | **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Gestione base di una partita in locale (interazioni Model-Controller) | Completamento meccaniche di fine partita | Leonardo Montini | 1 | 1 | 1 | 1 |  |  |  |
|  Sviluppo multiplayer peer-to-peer | Studio della dinamica di hosting di una partita | Leonardo Montini | 2 | 2 | 2 |  |  |  |  |
|   | Sviluppo dell'attore e test di connettività | Leonardo Montini | 3 | 3 | 2 | 2 | 2 |  |  |
|   | Progettazione e sviluppo del database Mongo (Atlas) | Giacomo Frisoni | 2 | 8 | 7 | 6 | 4 |  |  |
|  Gestione di una partita in remoto | Implementazione delle interazioni tra attori all'interno di una partita (invio di una mossa, ricezione di una mossa ecc.) | Sofia Rossi | 9 | 9 | 7 | 6 | 5 | 4 | 3 |
|   | Sviluppo del DAO per l'interazione col database richiesta da un attore | Giacomo Frisoni | 4 | 4 | 4 | 4 | 4 |  |  |
|  Sviluppo componente grafica della schermata di gioco | Creazione dei mockup | Marcin Pabich | 1 | 1 |  |  |  |  |  |
|   | Implementazione della schermata di gioco |  | 5 | 4 | 3 | 3 | 2 | 1 |  |