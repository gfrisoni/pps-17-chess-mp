|  **Sprint Task** | **Responsabile** | **Stima iniziale** | **1** | **2** | **3** | **4** | **5** | **6** |
|  :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: | :------: |
|  Gestione dell'interazione con la scacchiera | Marcin Pabich | 2 | 2 | 2 | 2 | 1 |  |  |
|  Visualizzazione dell'highlight (con enabling) | Marcin Pabich | 1 | 1 | 1 | 2 |  |  |  |
|  Visualizzazione delle board a seguito delle mosse compiute | Marcin Pabich | 1 | 1 |  |  |  |  |  |
|  Evidenziazione dell'ultima mossa effettuata | Marcin Pabich | 1 | 1 | 1 | 1 |  |  |  |
|  Visualizzazione delle informazioni sul pannello laterale | Marcin Pabich | 1 | 1 | 1 | 1 | 1 |  |  |
|  Navigazione e rappresentazione della history | Marcin Pabich | 3 | 3 | 3 | 3 | 3 | 3 | 3 |
|  Comportamento a fine partita | Leonardo Montini | 2 | 1 |  |  |  |  |  |
|  Salvataggio della PGN su database | Giacomo Frisoni | 1 | 1 | 1 | 1 | 1 | 1 |  |
|  Gestione della navigazione in avanti e in indietro lato controller | Sofia Rossi | 3 | 3 | 3 |  |  | 1 | 1 |
|  Raffinamento dello schema per i documenti rappresentanti un gioco | Giacomo Frisoni | 2 | 2 |  |  |  |  |  |
|  Gestione di tutte le interazioni necessarie lato controller | Sofia Rossi | 6 | 6 | 6 | 5 | 4 | 1 |  |